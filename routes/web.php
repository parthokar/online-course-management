<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\frontend\FrontendController;
use App\Http\Controllers\admin\CourseCategoryController;
use App\Http\Controllers\admin\CourseController;
use App\Http\Controllers\admin\QuizController;
use App\Http\Controllers\admin\ExamController;
use App\Http\Controllers\admin\AnnouncementController;
use App\Http\Controllers\admin\PromocodeController;
use App\Http\Controllers\admin\CourseEnrollController;
use App\Http\Controllers\frontend\StudentCourseEnrollController;
use App\Http\Controllers\admin\EmailController;
use App\Http\Controllers\admin\SmsController;
use App\Http\Controllers\admin\StudentLogController;
use App\Http\Controllers\admin\StudenPaymentController;
use App\Http\Controllers\frontend\StudentCourseEnroll;
use App\Http\Controllers\frontend\StudentAccountController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Http\Controllers\frontend\StudentMeetRequestController;
use App\Http\Controllers\admin\SlackController;
use App\Http\Controllers\admin\ExamQuestionController;
use App\Http\Controllers\admin\QuizQuestionController;
use App\Http\Controllers\admin\StudentExamController;
use App\Http\Controllers\admin\StudentQuizController;
use App\Http\Controllers\admin\StudentCertificateController;
use App\Http\Controllers\admin\UserManageController;
use App\Http\Controllers\admin\VideoController;
use App\Http\Controllers\admin\CompanyController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\admin\SystemSettingController;
use App\Http\Controllers\admin\PermissionController;
use App\Http\Controllers\admin\StuentRecordVideo;
use App\Http\Controllers\admin\NoticeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\Auth\ResetPasswordController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [FrontendController::class, 'index'])->name('home_route');
Route::get('/panel-student-course', [FrontendController::class, 'studentPanelCourse'])->name('p_s_c_route');
Route::get('/panel-student-course-details/{id}', [FrontendController::class, 'studentPanelCourseDetails'])->name('p_s_c_d_route');
Route::get('/panel-student-course-sylabus/{id}', [FrontendController::class, 'courseSylabus'])->name('c_s');


Route::get('/user-registration', [FrontendController::class, 'userRegistration'])->name('u_r');
Route::get('/user-reset-password', [FrontendController::class, 'userResetPassword'])->name('u_r_p');
Route::post('/user-reset-password-post', [FrontendController::class, 'userResetPasswordPost'])->name('u_r_p_post');
Route::get('/get-exam-result/{course_id}', [FrontendController::class, 'getExamResult'])->name('get_exam_result');
Route::get('/get-quiz-result/{course_id}', [FrontendController::class, 'getQuizResult'])->name('get_quiz_result');


Route::get('/panel-student-course-class-news/{id}', [FrontendController::class, 'classNews'])->name('c_n');
Route::get('/panel-student-course-recording/{id}', [FrontendController::class, 'courseRecording'])->name('c_r');
Route::get('/panel-student-course-test-quiz/{id}', [FrontendController::class, 'courseTestAndQuiz'])->name('c_t_q');
Route::get('/panel-student-course-meet-teacher/{id}', [FrontendController::class, 'courseMeetTeacher'])->name('c_m_t');


Route::get('/panel-student-course-payment-invoice/{id}', [FrontendController::class, 'coursePaymentInvoice'])->name('c_p_i');
Route::get('/panel-student-course-quiz/{course_id}/{quiz_id}', [FrontendController::class, 'courseQuiz'])->name('c_q');
Route::get('/panel-student-course-test/{course_id}/{test_id}', [FrontendController::class, 'courseTest'])->name('c_t');
Route::get('/panel-student-meet-teacher/{course_id}', [FrontendController::class, 'courseMeetTeacher'])->name('m_t');


Route::get('/panel-student-chat/{id}', [FrontendController::class, 'chatStudent'])->name('chat_student');
Route::get('/panel-student-chat-message/{id}/{course_id}', [FrontendController::class, 'chatStudentMessage'])->name('chat_student_message');
Route::get('/all-video', [FrontendController::class, 'allVideo'])->name('all_video_route');
Route::get('/about', [FrontendController::class, 'about'])->name('about_route');


Route::get('/announcement/list', [FrontendController::class, 'announcement'])->name('announcement_route');
Route::get('/contact', [FrontendController::class, 'contact'])->name('contact_route');
Route::get('/all-course', [FrontendController::class, 'allCourse'])->name('all_course_route');
Route::get('/faq', [FrontendController::class, 'faq'])->name('faq_route');


Route::get('/stu-record-video', [FrontendController::class, 'recordVideo'])->name('record_video_route');
Route::get('/course-details/{id}', [FrontendController::class, 'show'])->name('course_details');
Route::get('student-course-enroll', [StudentCourseEnroll::class, 'enrollNow'])->name('course_enroll_route');
Route::post('contact-store', [ContactController::class, 'contactStore'])->name('contact_store_route');


Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');


Route::resource('student-course-enroll', StudentCourseEnroll::class);

Route::group(['middleware' => 'auth'], function () {

    Route::resource('meet-instructor', StudentMeetRequestController::class);
    Route::get('student-certificate-view', [StudentMeetRequestController::class, 'certificate'])->name('s_certificate');
    Route::post('student-certificate-data', [StudentMeetRequestController::class, 'certificateData'])->name('s_certificate_d');
    Route::get('/exam/q/delete/{id}', [ExamController::class, 'examQDelete'])->name('exam_q_delete');

    Route::get('/quiz/q/delete/{id}', [QuizController::class, 'quizQDelete'])->name('quiz_q_delete');
    Route::get('/join/call', [SlackController::class, 'joinCall'])->name('video_call');
    Route::get('/slack/message/{id}', [SlackController::class, 'slackMessage'])->name('channel_message_route');
    Route::post('/slack/channel/store', [SlackController::class, 'channelStore'])->name('channel_route_store');

    Route::get('ajax/{id}', [SlackController::class, 'slackMessageAjax'])->name('cchannel_message_route_ajax');
    Route::post('data/ajax/store', [SlackController::class, 'slackMessageAjaxStore'])->name('a_message_route_store');
    Route::get('admin/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('cc-course-student/{courseId}', [StudentCertificateController::class, 'courseStudent']);


    Route::post('certificate-student-generate', [StudentCertificateController::class, 'generateCertificate'])->name('certificate_generate');
    Route::post('certificate-student-generate-quiz', [StudentCertificateController::class, 'generateCertificateQuiz'])->name('certificate_generate_quiz');

    Route::get('get-teacher/{role_id}', [UserManageController::class,'getTeacher'])->name('get_teacher');
    Route::get('get-student/{role_id}', [UserManageController::class,'getStudent'])->name('get_student');

    Route::group(['middleware' => 'role'], function () {

        Route::resource('notice', NoticeController::class);
        Route::resource('record-video', StuentRecordVideo::class);
        Route::resource('contact-student', ContactController::class);
        Route::resource('exam', ExamController::class);



        Route::resource('question-exam', ExamQuestionController::class);
        Route::resource('question-quiz', QuizQuestionController::class);
        Route::resource('video', VideoController::class);
        Route::resource('company', CompanyController::class);


        Route::get('/permission-list', [PermissionController::class, 'index'])->name('permission_list');
        Route::get('/permission-edit/{id}', [PermissionController::class, 'edit'])->name('permission_edit');
        Route::PATCH('/permission-update/{id}', [PermissionController::class, 'update'])->name('permission_update');
        Route::resource('manage-user', UserManageController::class);



        Route::resource('roles', RoleController::class);
        Route::resource('role_permissions', RolePermissionController::class)->only(['index', 'create', 'store', 'destroy']);
        Route::resource('course-category', CourseCategoryController::class);
        Route::resource('course', CourseController::class);


        Route::resource('quiz', QuizController::class);
        Route::resource('announcement', AnnouncementController::class);
        Route::resource('course-enroll', CourseEnrollController::class);
        Route::resource('send-email', EmailController::class);


        Route::resource('send-sms', SmsController::class);
        Route::resource('activity-log', StudentLogController::class);
        Route::resource('student-payment', StudenPaymentController::class);
        Route::get('payment-details/{student_id}/{course_id}', [StudenPaymentController::class, 'details'])->name('pay_details');


        Route::get('import-user', [UserManageController::class, 'import'])->name('import_user');
        Route::post('import-users', [UserManageController::class, 'importUsers'])->name('import.users');
        Route::get('promo-code', [CourseController::class, 'pindex'])->name('promo_index');
        Route::get('promo-code-create', [CourseController::class, 'pcreate'])->name('promo_create');


        Route::post('promo-code', [CourseController::class, 'pstore'])->name('promo_store');
        Route::get('promo-code/{id}', [CourseController::class, 'pedit'])->name('promo_edit');
        Route::PATCH('promo-code/{id}', [CourseController::class, 'pupdate'])->name('promo_update');



        Route::get('/slack/channel/create', [SlackController::class, 'channelCreate'])->name('channel_route_create');
        Route::get('/slack/channel', [SlackController::class, 'channelList'])->name('channel_route');
        Route::resource('student-exam', StudentExamController::class);
        Route::resource('student-quiz', StudentQuizController::class);


        Route::get('certificate-student', [StudentCertificateController::class, 'index'])->name('get_certificate');
        Route::get('certificate-student-quiz', [StudentCertificateController::class, 'indexQuiz'])->name('get_quiz');
        Route::resource('system-settings', SystemSettingController::class);
    });


});
