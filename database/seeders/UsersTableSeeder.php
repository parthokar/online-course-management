<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
               'name'=>'Admin',
               'email'=>'admin@email.com',
                'role_id'=>'1',
                'password'=> bcrypt('12345678'),
                'password'=> bcrypt('12345678'),
            ],
            [
               'name'=>'Student',
               'email'=>'student@email.com',
                'role_id'=>'2',
               'password'=> bcrypt('12345678'),
            ],
            [
                'name'=>'Teacher',
                'email'=>'teacher@email.com',
                 'role_id'=>'3',
                'password'=> bcrypt('12345678'),
             ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
