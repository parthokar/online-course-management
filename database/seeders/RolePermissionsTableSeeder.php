<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RolePermissionsTableSeeder extends Seeder
{
    public function run()
    {
        // Get roles and permissions by their names
        $adminRole = Role::where('name', 'admin')->first();
        $customerRole = Role::where('name', 'customer')->first();
        $managerRole = Role::where('name', 'manager')->first();

        $createUserPermission = Permission::where('name', 'create_user')->first();
        $editUserPermission = Permission::where('name', 'edit_user')->first();
        $deleteUserPermission = Permission::where('name', 'delete_user')->first();

        // Assign permissions to roles
        $adminRole->permissions()->attach([$createUserPermission->id, $editUserPermission->id, $deleteUserPermission->id]);
        $customerRole->permissions()->attach([$createUserPermission->id]);
        $managerRole->permissions()->attach([$editUserPermission->id, $deleteUserPermission->id]);
        // You can customize the assignment as per your requirements
    }
}
