<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('teacher_id');
            $table->unsignedBigInteger('promo_id')->nullable();
            $table->string('course_title');
            $table->string('tag_line');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('duration');
            $table->text('details');
            $table->text('contents');
            $table->text('requirements');
            $table->string('course_image');
            $table->string('currency');
            $table->string('live_class_url')->nullable();
            $table->decimal('course_price',18,2);
            $table->tinyInteger('free');
            $table->unsignedBigInteger('created_by');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
