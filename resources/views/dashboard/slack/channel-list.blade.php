@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Channel List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">

        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
     @endif

     @if(session()->has('error'))
     <div class="alert alert-danger">
         {{ session()->get('error') }}
     </div>
  @endif



        <div class="row">
            @foreach($channel as $channels)

                <div class="col-md-2">
                    <a href="{{route('channel_message_route',$channels->main_id)}}" style="color:black">
                        <div class="card">
                            <img class="img-fluid" width="100%" src="{{asset('dashboard/channel/slack.png')}}" alt="slack">
                            <div class="card-header text-center"><strong>{{$channels->channel_name}}</strong> </div>
                            <div class="card-body text-center">@if(isset($channels->course_title))Course: {{$channels->course_title}}@endif</div>
                            <div class="card-body text-center">Type:  @if($channels->channel_type==1) Restricted @else Open Channel @endif</div>
                        </div>
                    </a>
                </div>

            @endforeach
        </div>


      </div>
    </section>
  </div>
@endsection


