@extends('layouts.app')
@section('title')
    Teacher
@endsection
@section('content')

<style>
    /* CSS for the chat box container */
    .chat-box {
        list-style: none;
        padding: 0;
        margin: 0;
        display: flex;
        flex-direction: column;
    }

    /* CSS for individual chat messages */
    .chat-box li {
        padding: 8px;
        margin: 8px 0;
        max-width: 70%;
        border-radius: 8px;
        text-align: center;
    }

    /* Your messages (You) */
    .chat-box li.you {
        background-color: blueviolet;
        color: white;
        align-self: flex-end;
    }

    /* Other messages */
    .chat-box li.other {
        background-color: purple;
        color: white;
        align-self: flex-start;
    }

    /* CSS for chat message container */
    .chat-container {
        position: relative;
    }

    /* CSS for the textarea container */
    .textarea-container {
        position: relative;
    }

    /* Adjust padding for the message input to leave space for the button */
    #chat_message {
        padding-right: 80px;
    }

    /* CSS for the send button */
    .btn-primary {
        position: absolute;
        top: 0;
        right: 0;
        height: 100%;
        width: 80px; /* Adjust the button width as needed */
        border-radius: 0 8px 8px 0; /* Rounded corner on the right side */
    }
    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Channel Message</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">

                <input type="hidden" value="{{ $id }}" id="id">
                <h4>Channel name : {{$channel_info->channel_name}}</h4>
                <h3 class=" text-center">Messaging </h3>
                <div class="messaging">
                    <div class="row">

                        <div class="col-md-12">

                            <div id="chat_message_history" class="chat-box" style="background: #e7e7e7;

                            height: 300px;
                            overflow: scroll"></div>

                            <input type="hidden" id="slack_id" value="{{ $id }}">
                            <input type="hidden" id="login_id" value="{{ auth()->user()->id }}">
                            <div class="form-group mt-5 chat-container">
                                <div class="textarea-container">
                                    <textarea id="chat_message" class="form-control" name="message"></textarea>
                                </div>
                                <button onclick="messageList({{ $id }},{{ auth()->user()->id }})" type="button" class="btn btn-primary btn-sm">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        function messageList(id, authId) {
            var textarea = document.getElementById("chat_message");
            var message = $("#chat_message").val();
            if (message == '') {
                alert('Please enter your message');
                return false;
            }
            $("#reload_message").hide();
            axios.post('{{ route('a_message_route_store') }}', {
                    slack_id: id,
                    message: message,
                })
                .then(function(response) {
                    message.value='';
                    textarea.value = "";
                    $("#reload_message").hide();
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

        $(document).ready(function() {
            var parameterValue = $("#slack_id").val();
            var parameterValue2 = $("#login_id").val();
            setInterval(function() {
                messageData(parameterValue, parameterValue2);
            }, 2000);
        });


        function messageData(id, authId) {

    axios.get('{{ url('ajax/') }}' + "/" + id, {})
        .then(function(response) {
            var options = '';


            $.each(response.data, function(index, item) {

                var responseDateTime = item.created_at;

                var date = new Date(responseDateTime);

                var formattedDateTime = date.toLocaleString('en-BD', {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true
                });

                var message_info;
                if (item.user_id == authId) {
                    message_info = '<li class="you">You: '   + item.message +  "<br>" + formattedDateTime + '</li>';
                } else {
                    message_info = '<li class="other">' + item.user.name + ': ' + item.message + "<br>" + formattedDateTime + '</li>';
                }
                options += message_info;
            });
            $('#chat_message_history').html(options);
        })
        .catch(function(error) {
            console.log(error);
        });
}

    </script>
@endsection
@endsection
