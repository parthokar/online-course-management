@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course Update</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row container">
                    <h4 class="mt-3 mb-3">Course Update</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('course.update',$course->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{method_field('PATCH')}}
                        <div class="row container">
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Category: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="category_id" required>
                                        @foreach($categorys as $c)
                                         <option value="{{$c->id}}" @if($c->id==$course->category_id) selected @endif>{{$c->course_category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Course title: <span style="color:red">*</span></strong>
                                    <input type="text" name="course_title" value="{{$course->course_title}}" class="form-control" placeholder="Enter title" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Tag Line: <span style="color:red">*</span></strong>
                                    <input type="text" name="tag_line" value="{{$course->tag_line}}" class="form-control" placeholder="Tag line" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <strong>Start date: <span style="color:red">*</span></strong>
                                    <input type="date" onchange="updateDuration()" id="start-date-picker" name="start_date" value="{{$course->start_date}}" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>End date: <span style="color:red">*</span></strong>
                                    <input onchange="updateDuration()" type="date" id="end-date-picker" name="end_date" value="{{$course->end_date}}" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>Duration: <span style="color:red">*</span></strong>
                                    <input type="text" id="duration" name="duration" value="{{$course->duration}}" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>Prerequisite: <span style="color:red">*</span></strong>
                                    <input type="text" name="requirements" value="{{$course->requirements}}" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Course image: </strong>
                                    <img src="{{asset('dashboard/course/'.$course->course_image)}}" width="40px" height="30px" accept="image/png, image/gif, image/jpeg">
                                    <input type="file" name="course_image" class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Course price: <span style="color:red">*</span></strong>
                                    <div class="input-group">
                                        <select name="currency" class="form-control">
                                            <option value="BDT"@if($course->currency=='BDT') selected @endif>BDT</option>
                                            <option value="USD" @if($course->currency=='USD') selected @endif>USD</option>
                                            <option value="EUR" @if($course->currency=='EUR') selected @endif>EUR</option>
                                        </select>
                                        <input type="text" value="{{$course->course_price}}" name="course_price" class="form-control" placeholder="Course price" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Free Course: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="free" required>
                                        <option value="0" @if($course->free==0) selected @endif>No</option>
                                        <option value="1" @if($course->free==1) selected @endif>Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Assign Instructor: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="teacher_id" required>
                                        <option value="">Select Instructor</option>
                                        @foreach($teacher as $teachers)
                                          <option value="{{$teachers->id}}" @if($course->teacher_id==$teachers->id) selected @endif>{{$teachers->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Promocode:</strong>
                                    <select class="form-control" name="promo_id">
                                        <option value="">Select Promocode</option>
                                        @foreach($promocode as $promocodes)
                                          <option value="{{$promocodes->id}}" @if($course->promo_id==$promocodes->id) selected @endif>{{$promocodes->code}}-{{$promocodes->amount}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Status: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="status" required>
                                        <option value="1" @if($course->status==1) selected @endif>Active</option>
                                        <option value="0" @if($course->status==0) selected @endif>InActive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>Live Class Url:</strong>
                                    <input type="text" name="live_class_url" value="{{$course->live_class_url}}" class="form-control" >
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Details: <span style="color:red">*</span></strong>
                                    <textarea name="details" class="form-control" required>{{$course->details}}</textarea>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Contents: <span style="color:red">*</span></strong>
                                    <textarea name="contents" class="form-control" required>{{$course->contents}}</textarea>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
               </div>
          </div>
      </div>
    </section>
  </div>

@section('script')
<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
   <script>
     CKEDITOR.replace('details');
     CKEDITOR.replace('contents');
  function calculateDateDuration(startDate, endDate) {
    var start = new Date(startDate);
    var end = new Date(endDate);
    var duration = Math.abs(end - start);
    var days = Math.ceil(duration / (1000 * 60 * 60 * 24));
    return days+1;
  }

  function updateDuration() {
    var startDate = $('#start-date-picker').val();
    var endDate = $('#end-date-picker').val();
    if(startDate!='' && endDate!=''){
        var duration = calculateDateDuration(startDate, endDate);
        $('#duration').val(duration);
    }
  }

  // Add event listeners to the date pickers
  $('#start-date-picker').change(updateDuration);
  $('#end-date-picker').change(updateDuration);


    </script>
 @endsection
@endsection


