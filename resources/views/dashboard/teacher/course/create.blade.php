@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course Create</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row container">
                <div class="col-md-12">
                    <h4 class="mt-3 mb-3">Create Course</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('course.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                         <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Category: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="category_id" required>
                                        @foreach($category as $c)
                                         <option value="{{$c->id}}">{{$c->course_category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Course title: <span style="color:red">*</span></strong>
                                    <input type="text" value="{{old('course_title')}}" name="course_title" class="form-control" placeholder="Enter title" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Tag Line: <span style="color:red">*</span></strong>
                                    <input type="text" value="{{old('tag_line')}}" name="tag_line" class="form-control" placeholder="Tag line" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <strong>Start date: <span style="color:red">*</span></strong>
                                    <input onchange="updateDuration()" value="{{old('start_date')}}"  type="date" id="start-date-picker" name="start_date" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>End date: <span style="color:red">*</span></strong>
                                    <input onchange="updateDuration()" value="{{old('end_date')}}" type="date" id="end-date-picker" name="end_date" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>Duration: <span style="color:red">*</span></strong>
                                    <input type="text" value="{{old('duration')}}" name="duration"  id="duration" class="form-control" required>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2">
                                <div class="form-group">
                                    <strong>Prerequisite: <span style="color:red">*</span></strong>
                                    <input type="text" value="{{old('requirements')}}" name="requirements" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mt-2 mb-2">
                                <div class="form-group">
                                    <strong>Course image: <span style="color:red">*</span></strong>
                                    <input type="file" name="course_image" class="form-control" accept="image/png, image/gif, image/jpeg" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Course price: <span style="color:red">*</span></strong>
                                    <div class="input-group">
                                        <select name="currency" class="form-control">
                                            <option value="USD">USD</option>
                                            <option value="BDT">BDT</option>
                                            <option value="EUR">EUR</option>
                                        </select>
                                        <input type="text" value="{{ old('course_price') }}" name="course_price" class="form-control" placeholder="Course price" required>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Free Course: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="free" required>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Assign Instructor: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="teacher_id" required>
                                        <option value="">Select Instructor</option>
                                        @foreach($teacher as $teachers)
                                          <option value="{{$teachers->id}}">{{$teachers->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Promocode:</strong>
                                    <select class="form-control" name="promo_id">
                                        <option value="">Select Promocode</option>
                                        @foreach($promocode as $promocodes)
                                          <option value="{{$promocodes->id}}">{{$promocodes->code}}-{{$promocodes->amount}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Status: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="status" required>
                                        <option value="0">InActive</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Details: <span style="color:red">*</span></strong>
                                    <textarea name="details" class="form-control" required>{{old('details')}}</textarea>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Contents: <span style="color:red">*</span></strong>
                                    <textarea name="contents" class="form-control" required>{{old('contents')}}</textarea>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </section>
  </div>



 @section('script')
 <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
   <script>
     CKEDITOR.replace('details');
     CKEDITOR.replace('contents');
  function calculateDateDuration(startDate, endDate) {
    var start = new Date(startDate);
    var end = new Date(endDate);
    var duration = Math.abs(end - start);
    var days = Math.ceil(duration / (1000 * 60 * 60 * 24));
    return days+1;
  }

  function updateDuration() {
    var startDate = $('#start-date-picker').val();
    var endDate = $('#end-date-picker').val();
    if(startDate!='' && endDate!=''){
        var duration = calculateDateDuration(startDate, endDate);
        $('#duration').val(duration);
    }
  }

  // Add event listeners to the date pickers
  $('#start-date-picker').change(updateDuration);
  $('#end-date-picker').change(updateDuration);


    </script>
 @endsection

@endsection


