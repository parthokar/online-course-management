@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">
         @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
         @endif

         <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Tag Line</th>
                        <th>Start date</th>
                        <th>End date</th>
                        <th>Duration</th>
                        <th>Instructor</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
    </section>
  </div>

  @section('script')

  <script>
    $(document).ready(function() {
               $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('course.index') }}",
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {data: 'category', name: 'category'},
                {data: 'course_title', name: 'course_title'},
                {data: 'tag_line', name: 'tag_line'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date', name: 'end_date'},
                {data: 'duration', name: 'duration'},
                {data: 'created', name: 'created'},
                {data: 'course_price', name: 'course_price'},
                {data: 'image', name: 'image'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
  </script>
  @endsection
@endsection


