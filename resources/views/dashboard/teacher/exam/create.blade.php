@extends('layouts.app')
@section('title')
    Teacher
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Create Exam</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="container-fluid card">
                    <form action="{{ route('exam.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="mt-3 mb-3">Create Exam</h4>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                    <div class="form-group">
                                        <strong>Select Course: <span style="color:red">*</span></strong>
                                        <select class="form-control" name="course_id" required>
                                            <option value="">Select Course</option>
                                            @foreach ($course as $item)
                                                <option value="{{ $item->id }}" @if($item->id==old('course_id')) selected @endif>{{ $item->course_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                    <div class="form-group">
                                        <strong>Title: <span style="color:red">*</span></strong>
                                        <input type="text" value="{{old('title')}}" name="title" class="form-control" placeholder="Title"
                                            required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                    <div class="form-group">
                                        <strong>Exam date: <span style="color:red">*</span></strong>
                                        <input type="date" value="{{old('exam_date')}}" name="exam_date" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                    <div class="form-group">
                                        <strong>Exam time: <span style="color:red">*</span></strong>
                                        <input type="time" value="{{old('exam_time')}}" name="exam_time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                    <div class="form-group">
                                        <strong>Details: <span style="color:red">*</span></strong>
                                        <textarea name="details" class="form-control" required>{{old('details')}}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Create Exam</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4 class="mt-3 mb-3">Create Question</h4>
                                    <div id="questions-container">
                                        <!-- Dynamic question fields will be added here -->
                                    </div>
                                    <button type="button" class="btn btn-primary" id="add-question">Add Question
                                        +</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const questionsContainer = document.getElementById("questions-container");
            const addQuestionButton = document.getElementById("add-question");
            let questionCount = 0;
            function createQuestionField() {
                questionCount++;
                const questionDiv = document.createElement("div");
                questionDiv.innerHTML = `
            <div class="mb-3">
                <label for="question${questionCount}" class="form-label">Question ${questionCount}:</label>
                <input type="text" class="form-control" name="questions[${questionCount}]" required>
                <button type="button" class="remove-question btn btn-danger mt-2">x</button>
                <div class="options-container mt-3">
                    <label class="form-label">Option Type:</label>
                    <select name="option_type[${questionCount}]" class="form-select form-control" required>
                        <option value="textbox">Text Box</option>
                        <option value="option" selected>Option</option>
                    </select>
                    <div class="row">
                        <!-- Option input fields will be added here in columns -->
                    </div>
                </div>
                <button type="button" class="add-option btn btn-primary mt-2">Add Option</button>
                <div class="answer-container mt-3">
                    <!-- Answer input field will be added here based on the option type -->
                </div>
            </div>
        `;
                questionsContainer.appendChild(questionDiv);
                const removeQuestionButton = questionDiv.querySelector(".remove-question");
                removeQuestionButton.addEventListener("click", function() {
                    questionsContainer.removeChild(questionDiv);
                    questionCount--;
                });
                const addOptionButton = questionDiv.querySelector(".add-option");
                const optionsContainer = questionDiv.querySelector(".options-container");
                const optionTypeSelect = questionDiv.querySelector("select[name^='option_type']");
                const answerContainer = questionDiv.querySelector(".answer-container");
                // Function to show/hide the add-option button and answer input field
                function toggleOptionsAndAnswer() {
                    const value = optionTypeSelect.value;
                    if (value === 'textbox') {
                        addOptionButton.style.display = "none";
                        answerContainer.innerHTML =
                            `<input type="text" name="answer_new[${questionCount}][]" class="form-control" required>`;
                    } else {
                        addOptionButton.style.display = "block";
                        answerContainer.innerHTML =
                            `<select name="answers[${questionCount}][]" class="form-select form-control" required></select>`;
                    }
                }
                // Add event listener to the Option Type dropdown
                optionTypeSelect.addEventListener("change", toggleOptionsAndAnswer);
                addOptionButton.addEventListener("click", function() {
                    optionsContainer.style.display = "block";
                    answerContainer.style.display = "block";
                    const row = document.createElement("div");
                    row.classList.add("row");
                    // Add a new column for the option input field
                    const column = document.createElement("div");
                    column.classList.add("col-md-3");
                    const optionInput = document.createElement("input");
                    optionInput.type = "text";
                    optionInput.name = `options[${questionCount}][]`;
                    optionInput.required = true;
                    optionInput.classList.add("form-control", "option-input");
                    column.appendChild(optionInput);
                    // Add a remove option button (X button) in the same column
                    const removeOptionButton = document.createElement("button");
                    removeOptionButton.type = "button";
                    removeOptionButton.className = "remove-option btn btn-danger mt-2";
                    removeOptionButton.innerHTML = "&times;"; // "X" symbol
                    column.appendChild(removeOptionButton);
                    // Add the column to the row
                    row.appendChild(column);
                    // Add the row to the options container
                    optionsContainer.appendChild(row);
                    // Add the option to the answer select
                    updateAnswerSelect(questionDiv);
                    // Attach event listener to remove option button
                    removeOptionButton.addEventListener("click", function() {
                        row.removeChild(column);
                        updateAnswerSelect(questionDiv);
                    });
                });
                // Initially, set the visibility of add-option button and answer input field
                toggleOptionsAndAnswer();
            }
            function updateAnswerSelect(questionDiv) {
                const answerSelect = questionDiv.querySelector(".answer-container select");
                const optionInputs = questionDiv.querySelectorAll(".option-input");
                // Clear existing options
                answerSelect.innerHTML = '';
                // Add options based on the option input fields
                optionInputs.forEach(function(optionInput, index) {
                    const optionValue = optionInput.value.trim();
                    if (optionValue !== '') {
                        const newOption = document.createElement("option");
                        newOption.value = optionValue;
                        newOption.text = optionValue;
                        answerSelect.appendChild(newOption);
                    }
                });
            }
            addQuestionButton.addEventListener("click", createQuestionField);
        });
    </script>
@endsection
@endsection
