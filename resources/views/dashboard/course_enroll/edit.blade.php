@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course Category</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="mt-3 mb-3">Update Course Enroll</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('course-enroll.update',$enroll->id) }}" method="POST">
                        @csrf
                         {{method_field('PATCH')}}
                         <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Student: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="student_id" required>
                                        <option value="">Select Student</option>
                                        @foreach($student as $students)
                                          <option value="{{$students->id}}" @if($students->id==$enroll->student_id) selected @endif>{{$students->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Course: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="course_id" required>
                                        <option value="">Select Course</option>
                                        @foreach($course as $courses)
                                          <option value="{{$courses->id}}" @if($courses->id==$enroll->course_id) selected @endif>{{$courses->course_title}}</option>
                                       @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Promo:</strong>
                                    <select class="form-control" name="promo_id">
                                        <option value="">Select Promocode</option>
                                        @foreach($promo as $promos)
                                          <option value="{{$promos->id}}" @if($promos->id==$enroll->promo_id) selected @endif>{{$promos->code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Status: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="status" required>
                                        <option value="1" @if($enroll->status==1) selected @endif>Active</option>
                                        <option value="0" @if($enroll->status==0) selected @endif>InActive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection


