@extends('layouts.app')
@section('title')
    Teacher
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Create Channel</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h4 class="mt-3 mb-3">Create Channel</h4>

                            <form action="{{ route('channel_route_store') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                        <div class="form-group">
                                            <strong>Select Course:</strong>
                                            <select class="form-control" name="course_id">
                                                <option value="">Select Course</option>
                                                @foreach ($course as $c)
                                                    <option value="{{ $c->id }}">{{ $c->course_title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                        <div class="form-group">
                                            <strong>Channel Name: <span style="color:red">*</span></strong>
                                            <input type="text" name="channel_name" value="" class="form-control"
                                                placeholder="Channel Name" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                        <div class="form-group">
                                            <strong>Channel Type: <span style="color:red">*</span></strong>
                                            <select class="form-control" name="channel_type">
                                                <option value="1">Restricted</option>
                                                <option value="2">Open Channel</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
