@extends('layouts.app')
@section('title')
    Create User
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Create Role</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h4 class="mt-3 mb-3">Create Role</h4>
                            {!! Form::open(['route' => 'roles.store', 'method' => 'POST']) !!}
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }} <span
                                        style="color:red">*</span></label>

                                <div class="col-md-6">
                                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3 mt-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Permission Name</th>
                                        <th>Description</th>
                                    </tr>
                                    @foreach ($permission as $value)
                                        <tr>
                                            <td> <label>{{ Form::checkbox('permissions[]', $value->url, false, ['class' => 'name']) }}
                                                    {{ $value->name }}</label></td>
                                            <td>{{ $value->description }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
