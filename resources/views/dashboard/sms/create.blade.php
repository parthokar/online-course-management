@extends('layouts.app')
@section('title')
    Email
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Send Email</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="row container">
                        <div class="col-md-12">
                            <h4 class="mt-3 mb-3">Send Email</h4>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            <form method="post" action="{{ route('send-email.store') }}">
                                @csrf
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                        <div class="form-group">
                                            <strong>Select Option:</strong>
                                            <select id="option" class="form-control" required>
                                                <option value="">Select</option>
                                                <option value="1">Role Wise</option>
                                                <option value="2">Course Wise</option>
                                                <option value="3">Single Email</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-3 mb-2" id="role_wise" style="display: none">
                                        <div class="form-group">
                                            <strong>Select Role:</strong>
                                            <select class="form-control" name="role_id">
                                                <option value="0">All</option>
                                                @foreach ($role as $roles)
                                                    <option value="{{ $roles->id }}">{{ $roles->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-12 col-md-3 mb-2" id="course_wise" style="display: none">
                                        <div class="form-group">
                                            <strong>Select Course:</strong>
                                            <select class="form-control" name="course_id">
                                                <option value="0">All</option>
                                                @foreach ($course as $courses)
                                                    <option value="{{ $courses->id }}">{{ $courses->course_title }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-3 mb-2" id="single_wise" style="display: none">
                                        <div class="form-group">
                                            <strong>To:</strong>
                                            <input type="email" name="email" class="form-control"
                                                placeholder="Enter email address">
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                        <div class="form-group">
                                            <strong>Subject: <span style="color:red">*</span></strong>
                                            <input type="text" name="subject" class="form-control"
                                                placeholder="Enter subject" required>
                                        </div>
                                    </div>



                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                        <div class="form-group">
                                            <strong>Body: <span style="color:red">*</span></strong>
                                            <textarea cols="5" rows="5" name="body" class="form-control" required></textarea>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                        <button type="submit" class="btn btn-primary">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@section('script')
    <script>
        $("#option").change(function() {
            if ($("#option").val() == 1) {
                $("#role_wise").show();
            }else{
                $("#role_wise").hide();
            }

            if ($("#option").val() == 2) {
                $("#course_wise").show();
            }else{
                $("#course_wise").hide();
            }

            if ($("#option").val() == 3) {
                $("#single_wise").show();
            }else{
                $("#single_wise").hide();
            }
        });
    </script>
@endsection
@endsection
