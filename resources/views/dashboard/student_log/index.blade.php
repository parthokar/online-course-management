@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Student Log</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">
         @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
         @endif

         <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Subject</th>
                        <th>Student name</th>
                        <th>Url</th>
                        <th>Method</th>
                        <th>Ip</th>
                        <th>Agent</th>
                        <th>Time</th>
                        {{-- <th width="100px">Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
    </section>
  </div>

  @section('script')

  <script>
    $(document).ready(function() {
               $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('activity-log.index') }}",
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {data: 'subject', name: 'subject'},
                {data: 'students', name: 'students'},
                {data: 'url', name: 'url'},
                {data: 'method', name: 'method'},
                {data: 'ip', name: 'ip'},
                {data: 'agent', name: 'agent'},
                {data: 'created', name: 'created'},

                ]
            });
        });
  </script>
  @endsection
@endsection


