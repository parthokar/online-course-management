@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Take Exam</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="card-header">Course: {{$course->course_title}} <br> Exam: {{$exam->title}}  <br> Exam end date time: {{date('Y-m-d H:i a',strtotime($exam->exam_date.$exam->exam_time))}} </div>
            <div class="card-body">
                <form method="post" action="{{route('student-exam.store')}}">
                  @csrf
                  <input type="hidden" name="course_id" value="{{$course->id}}">
                  <input type="hidden" name="exam_id" value="{{$exam->id}}">
                  <input type="hidden" name="exam_name" value="{{$exam->title}}">
                  <input type="hidden" name="exam_time" value="{{$exam->exam_date.' '.$exam->exam_time}}">


                    <table class="table table-bordered">
                    <tr>
                            <th>Sl</th>
                            <th>Question</th>
                            <th>Option</th>
                    </tr>
                    @foreach($data as $key=> $questions)
                    <tr>
                            <td>{{++$key}}</td>
                            <td>
                                {{$questions->question}}
                                <input type="hidden" name="question_id[]" value="{{$questions->id}}">
                                <input type="hidden" name="question[]" value="{{$questions->question}}">
                            </td>
                            <td>
                                @foreach(json_decode($questions->options) as $option)
                                <input type="radio" id="option_{{ $questions->id }}_{{ $loop->index }}" name="answer_{{ $questions->id }}" value="{{ $option }}">
                                <label for="option_{{ $questions->id }}_{{ $loop->index }}">{{ $option }}</label><br>
                                @endforeach
                            </td>
                    </tr>
                    @endforeach
                    </table>
                 <button type="submit" class="btn btn-success">Submit Answer</button>
                </form>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection


