@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
<style>
    /* Card Styles */
    .card {
        width: 400px;
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 20px;
        margin: 20px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        background-color: #f9f9f9;
    }

    .card h2 {
        margin-bottom: 10px;
        color: #333;
    }

    /* Table Styles */
    .payment-table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

    .payment-table th, .payment-table td {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: left;
    }

    .payment-table th {
        background-color: #f2f2f2;
    }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course Payment</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        {{-- <div class="card">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="mt-3 mb-3">Student Payment</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif


                    <span>Student: {{$data[0]->student->name}} </span> <br>
                    <span>Course: {{$data[0]->course->course_title}}</span> <br>


                </div>
                <div class="col-md-3"></div>
            </div>
        </div> --}}









        <div class="card" style="width: 100%">
            <h2>Student Details</h2>
            <p><strong>Name:</strong> {{$data[0]->student->name}}</p>
            <p><strong>Email:</strong> {{$data[0]->student->email}}</p>
            <!-- Add more student details here if needed -->

            <h2>Course Details</h2>
            <p><strong>Course Title:</strong> {{$data[0]->course->course_title}}</p>
            <p><strong>Course Price:</strong> {{$data[0]->course->course_price}}</p>
            <!-- Add more course details here if needed -->

            <h2>Payment List</h2>
            <table class="payment-table">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Transaction id</th>
                        <th>Payment date</th>
                        <th>Payment amount</th>
                        <th>Payment by</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($data as $key=>$item)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$item->transaction_id}}</td>
                        <td>{{date('F-d-Y h:i a',strtotime($item->created_at))}}</td>
                        <td>{{$data[0]->course->currency}} {{number_format($item->amount, 2, '.', ',')}} </td>
                        <td>{{optional($item->paymentBy)->name}}</td>
                    </tr>
                    @endforeach
                    <tfoot>
                        <tr>
                            <th>Course Price:</th>
                            <th id="total-payment">{{$data[0]->course->currency}} {{number_format($data[0]->course->course_price, 2, '.', ',')}}</th>
                        </tr>
                        <tr>
                            <th>Total Payment:</th>
                            <th id="total-payment">{{$data[0]->course->currency}} {{number_format($total_amount, 2, '.', ',')}} </th>
                        </tr>
                        <tr>
                            <th>Total Due:</th>
                            <th id="total-due">{{$data[0]->course->currency}} {{number_format($data[0]->course->course_price-$total_amount, 2, '.', ',')}} </th>
                        </tr>
                    </tfoot>

                </tbody>
            </table>
        </div>













      </div>
    </section>
  </div>
@endsection


