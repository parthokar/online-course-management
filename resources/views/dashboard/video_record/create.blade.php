@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Video</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="mt-3 mb-3">Create Video</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('record-video.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                         <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Course:</strong>
                                    <select class="form-control" name="course_id" required>
                                        <option value="">Select Course</option>
                                        @foreach($course as $c)
                                         <option value="{{$c->id}}">{{$c->course_title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Video Title:</strong>
                                    <input type="text" class="form-control" name="video_title" required>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Video:</strong>
                                    <input type="file" class="form-control" name="video" accept="video/*">
                                </div>
                            </div>

                            {{-- <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Video Url:</strong>
                                    <input type="text" class="form-control" name="video_url" required>
                                </div>
                            </div> --}}

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Recording Date: <span style="color:red">*</span></strong>
                                    <input type="datetime-local" name="publish_date" class="form-control" placeholder="Expire date" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection


