<!DOCTYPE html>
<html lang="en">
<head>
  <title>Quiz Result</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container-fluid p-5 bg-primary text-white text-center">
  <h1>Quiz Result</h1>
  <p class="font-weight:bold">Course: {{$course->course_title}} <br> Quiz: {{$exam->title}} - Date time: {{date('d-m-Y h:i a',strtotime($exam->quiz_date.$exam->quiz_time))}}  of <br>  {{$student->name}}</p>
  <p class="font-weight:bold">Total Question: {{$data->count()}} </p>
  <p class="font-weight:bold">Correct Answer: {{$score}} </p>
  <p class="font-weight:bold">Wrong Answer: {{$wrong}} </p>
  <p class="font-weight:bold">Your Score: {{$score}} </p>
</div>

<div class="container mt-5">
    <table class="table">
        <thead>
          <tr>
            <th>Sl</th>
            <th>Question</th>
            <th>Your answer</th>
            <th>Corrent answer</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>

          @foreach($data as $key=>$exams)
          <tr>
            <td>{{++$key}}</td>
            <td>{{$exams->question}}</td>
            <td>{{$exams->student_answer}}</td>
            <td>{{$exams->question_answer}}</td>
            <td>{{$exams->total_correct_answers}}</td>
          </tr>
         @endforeach

        </tbody>
      </table>
</div>


</body>
</html>

