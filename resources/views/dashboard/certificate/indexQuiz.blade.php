@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Quiz Result </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="mt-3 mb-3">Generate Quiz Result</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif

                    <form action="{{ route('certificate_generate_quiz') }}" method="POST">
                        @csrf
                         <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Course: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="course_id" id="course_id" required>
                                        <option value="">Select Course</option>
                                        @foreach($course as $courses)
                                          <option value="{{$courses->id}}">{{$courses->course_title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Student: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="student_id" id="student_id" required>
                                        <option value="">Select Student</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Select Month: <span style="color:red">*</span></strong>
                                    <input type="month" class="form-control" name="year_month" required>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">View Result </button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </section>
  </div>

@section('script')
  <script>
     $("#course_id").change(function(){
         var id = $("#course_id").val();
         $.ajax({
         url: '{{url("cc-course-student/")}}'+"/"+id,
         method: 'GET',

         success: function(response) {
             if(response.length > 0){
                 var options = 'Select Student';
                 $.each(response, function(index, item) {
                     options += '<option value="' + item.id + '">' + item.name + '</option>';
                 });
                 $('#student_id').html(options);
             }else{
                 alert('Sorry no student found');
                 var options = 'Select Student';
                 $('#student_id').html(options);
             }
         },
         error: function(xhr, status, error) {
             console.error(error);
         }
         });
     });
  </script>
@endsection
@endsection


