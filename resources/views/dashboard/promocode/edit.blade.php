@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Update Promo Code</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="mt-3 mb-3">Update Promo Code</h4>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('promo_update',$category->id) }}" method="POST">
                        @csrf
                         {{method_field('PATCH')}}
                         <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Code: <span style="color:red">*</span></strong>
                                    <input type="text" name="code" class="form-control" value="{{$category->code}}" required>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Amount: <span style="color:red">*</span></strong>
                                    <div class="input-group">
                                        <select name="currency" class="form-control">
                                            <option value="BDT"@if($category->currency=='BDT') selected @endif>BDT</option>
                                            <option value="USD" @if($category->currency=='USD') selected @endif>USD</option>
                                            <option value="EUR" @if($category->currency=='EUR') selected @endif>EUR</option>
                                        </select>
                                        <input type="text" value="{{$category->amount}}" name="amount" class="form-control" placeholder="amount" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <strong>Expire Date: <span style="color:red">*</span></strong>
                                    <input type="datetime-local" name="date_time" class="form-control" value="{{$category->date_time}}" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Status: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="status" required>
                                        <option value="1" @if($category->status==1) selected @endif>Active</option>
                                        <option value="0" @if($category->status==0) selected @endif>InActive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection


