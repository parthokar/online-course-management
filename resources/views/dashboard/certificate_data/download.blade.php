<!DOCTYPE html>
<html>
<head>
<title>Certificate</title>
<link rel="stylesheet" href="{{asset('frontend/css/certificate_style.css')}}">
</head>
<body>

    <section class="section">
        <div class="w-layout-blockcontainer container w-container">
            <div class="full_width"><div class="certificate-wrapper">
                <img src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64ea2d317cdf72d1a1fcf046_Logo%206%20(transparent).png" loading="lazy" sizes="(max-width: 479px) 63vw, (max-width: 767px) 56vw, 350px" srcset="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64ea2d317cdf72d1a1fcf046_Logo%206%20(transparent)-p-500.png 500w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64ea2d317cdf72d1a1fcf046_Logo%206%20(transparent).png 740w" alt="" class="certificate-logo">
                <h1 class="certificate-heading">Certification<br>
                    <span class="certificate-heading-span">
                        of Complition</span></h1><div class="certificate-text">
                            This certificate is proudly awarded to
                        </div>
                            <h2 class="certificate-student-name">{{$users->name}}</h2>
                            <div class="certificate-text">For successfully completing
                                <strong>{{$course_data->course_title}}</strong>
                                and obtaining the qualification to be a {{$course_data->course_title}}.
                            </div><div class="flex-space-between">
                                <div class="flex-width-1-3 flex-vr-center">
                                    <h4 class="certificate-date-sign">Date</h4>
                                </div><div class="flex-width-1-3 flex-vr-center">
                                    <img src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64ea342beab444bc09ab47f5_award.png" loading="lazy" alt="" class="image-wrapper">
                                </div>
                                <div class="flex-width-1-3 flex-vr-center">
                                    <h4 class="certificate-date-sign">Signature</h4>
                                </div></div><div class="certificate-footer">EXPERT AUTOMATION TEAM</div>
                            </div>
                        </div>
                    </div>
                </section>

    <script>
        window.print();
    </script>
</body>
</html>
