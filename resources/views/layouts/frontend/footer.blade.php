<div class="section cc-footer cc-home-2">
    <div class="container w-container">
        <div class="flex-space-between">
            <div class="flex-width-1-4">
                <h4 class="heading-h5 text-white">NAVIGATIONS</h4><a href="#" class="footer-link">Practice</a><a
                    href="#" class="footer-link">Http status code</a><a href="#" class="footer-link">Git
                    commands</a><a href="#" class="footer-link">DB query</a>
            </div>
            <div class="flex-width-1-4">
                <h4 class="heading-h5 text-white">IMPORTANT LINKS</h4><a href="#" class="footer-link">Automation
                    testing</a><a href="#" class="footer-link">API testing</a><a href="#"
                    class="footer-link">Windows command Line</a><a href="#" class="footer-link">linus command
                    line</a>
            </div>
            <div class="flex-width-1-4">
                <h4 class="heading-h5 text-white">CONTACT US</h4>
                <div class="paragraph cc-footer"><span class="icon-font"></span> +1 646-321-7507</div>
                <div class="paragraph cc-footer"><span class="icon-font"></span> info@expertautomationteam.com
                </div>
            </div>
            <div class="flex-width-1-4 cc-vr-left">
                <h4 class="heading-h5 text-white">CATCH US ON</h4>
                <div class="flex-space-between cc-start"><a href="#" class="footer-link cc-social bg-orange"><span
                            class="icon-brand"></span></a><a href="#" class="footer-link cc-social bg-orange"><span
                            class="icon-brand"></span></a><a href="#" class="footer-link cc-social bg-orange"><span
                            class="icon-brand"></span></a></div>





                            <a href="#"
                    class="button-df bg-orange cc-footer">Join Our Forum</a>
                    <a href="{{route('faq_route')}}"
                    class="button-df bg-orange cc-footer">FAQ</a>
            </div>
            <div class="divider-full cc-muted"></div>
            <div class="full_width">
                <div class="paragraph cc-topbar">© {{date('Y')}} . Copyright <strong>Expert Automation Team</strong>. All
                    Rights Reserved.</div>
            </div>
        </div>
    </div>
</div><a href="#navbar" class="back-to-top w-inline-block">
    <div><span class="icon-font"></span></div>
</a>






<div id="modal_1" class="g-modal_wrap cc-center" style="display: none;">
    <div id="g-modal_overlay" data-w-id="e38800ee-635f-ca87-185c-f79bdf274c46" class="g-modal_overlay cc-center" style="opacity: 0;"></div>
    <div id="modal-box" class="g-modal_box cc-center" style="opacity: 0;">
      <a id="modal-close" data-w-id="e38800ee-635f-ca87-185c-f79bdf274c48" href="#" class="g-modal_x w-inline-block"></a>
      <div class="mail-pop-head u-text-center">
        <img src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent.png" loading="lazy" sizes="(max-width: 479px) 63vw, 150px" srcset="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent-p-500.png 500w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent.png 549w" alt="" class="image-wrapper cc-modal">
        <h2 class="pop-heading">Sign In to Access your Account</h2>
      </div>
      <div class="mail-pop-body u-text-center">
        <div class="w-form">
            <form action="{{ route('login') }}" id="wf-form-Login-Form" name="wf-form-Login-Form" data-name="Login Form" method="post"
            class="text-center" data-wf-page-id="5d58db2bdc65b87dd3ff4c38"
            data-wf-element-id="b3a33c0d-690b-b88e-4619-e46a0483c544">
            @csrf
            <label for="Login-email" class="form-label">Email Address</label>
            <input type="email" class="w-input" autofocus="true" maxlength="256"  name="login_email" data-name="Login email" placeholder="Your Email" id="Login-email" required="">
            <label for="login_password" class="form-label">Password</label>
            <input type="password" class="w-input" autofocus="true" maxlength="256" name="login_password" data-name="login_password" placeholder="Enter Your Password" id="login_password" required="">
            <input type="submit" value="Login" data-wait="Please wait..." class="button-df w-button">
          </form>
          <div class="flex-space-between mt-20">
            <a href="{{route('u_r')}}" class="reset-reg-link">Create account</a>
            <a href="{{route('u_r_p')}}" class="reset-reg-link">Reset password</a>
          </div>
          <div class="w-form-done" tabindex="-1" role="region" aria-label="Login Form success">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail" tabindex="-1" role="region" aria-label="Login Form failure">
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
        </div>
      </div>
    </div>
  </div>

















<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5d58db2bdc65b81d7aff4c37"
    type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
<script src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/js/webflow.2cff4aca7.js"
    type="text/javascript"></script>
<script src="{{asset('frontend/js/custom.js')}}"
    type="text/javascript">
</script>

@yield('script')
