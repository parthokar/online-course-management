<div class="section cc-topbar">
    <div class="container w-container">
        <div class="flex-space-between">
            <div class="flex-width-2-3">
                <div class="paragraph cc-topbar"><span class="icon-font"></span> +1 646-321-7507</div>
                <div class="paragraph cc-topbar"><span class="icon-font"></span> info@expertautomationteam.com
                </div>
            </div>
            <div class="flex-width-1-3 text-right"><a href="#" class="paragraph cc-topbar cc-link">Login</a>
                <div class="paragraph cc-topbar cc-divider">|</div><a href="#"
                    class="paragraph cc-topbar cc-link">Register</a>
            </div>
        </div>
    </div>
</div>
