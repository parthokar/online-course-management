<div data-animation="over-right" class="g-nav cc-home-2 w-nav" data-easing2="ease-out" data-easing="ease-in"
    data-collapse="medium" role="banner" data-no-scroll="1" data-duration="400" id="navbar">
    <div class="g-nav_container"><a href="{{ route('home_route') }}" aria-current="page"
            class="g-brand w-nav-brand w--current">
            <div class="g-brand-logo_wrapper"><img
                    src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent.png"
                    loading="lazy"
                    srcset="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent-p-500.png 500w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent.png 549w"
                    sizes="(max-width: 991px) 70px, 100px" alt="" class="g-brand-logo" /></div>
        </a>
        <nav role="navigation" class="g-nav_menu w-nav-menu">
            <div class="g-nav_menu-mobile-flex cc-space-between">
                <div class="g-nav_menu-button cc-mobile-nav_menu-button w-nav-button"><img
                        src="https://uploads-ssl.webflow.com/640c3fe3d3b0bfea2c2d3553/640c3fe3d3b0bf6fcf2d35fd_icn-cross.svg"
                        loading="lazy" alt="" /></div>
                <ul role="list" class="g-nav_menu-list w-list-unstyled">
                    <li class="g-nav_menu-list_item cc-first-item">
                        <div data-hover="true" data-delay="0" class="g-nav_menu-dropdown_wrapper w-dropdown">
                            <div class="g-nav_menu-dropdown_toggle w-dropdown-toggle">
                                <div>courses</div>
                                <div class="g-nav_menu-dropdown-icon w-icon-dropdown-toggle"></div>
                            </div>
                            @php
                                $course = DB::table('courses')
                                    ->select('id', 'course_title')
                                    ->get();
                            @endphp
                            <nav class="g-nav_menu-dropdown cc-mobile-nav_menu-dropdown w-dropdown-list">
                                <ul role="list" class="g-nav_menu-dropdown_list w-list-unstyled">
                                    @foreach ($course as $item)
                                        <li class="g-nav_menu-dropdown-list_item"><a
                                                href="{{ route('course_details', $item->id) }}"
                                                class="g-nav_menu-dropdown-list_link w-dropdown-link">{{ $item->course_title }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </li>

                    <li class="g-nav_menu-list_item"><a href="{{ route('about_route') }}"
                            class="g-nav_menu-link_wrapper cc-mobile-nav_menu-link_wrapper w-nav-link">About Us</a></li>
                    <li class="g-nav_menu-list_item"><a href="{{ route('announcement_route') }}"
                            class="g-nav_menu-link_wrapper cc-mobile-nav_menu-link_wrapper w-nav-link">
                            Announcement</a></li>

                            {{-- <li class="g-nav_menu-list_item"><a href="{{route('record_video_route')}}"
                                class="g-nav_menu-link_wrapper cc-mobile-nav_menu-link_wrapper w-nav-link">
                                Record Video</a></li> --}}

                    <li class="g-nav_menu-list_item"><a href="{{ route('faq_route') }}"
                            class="g-nav_menu-link_wrapper cc-mobile-nav_menu-link_wrapper w-nav-link">
                            Faq</a>
                    </li>



                    <li class="g-nav_menu-list_item">
                        <div data-hover="true" data-delay="0" class="g-nav_menu-dropdown_wrapper w-dropdown">
                            <div class="g-nav_menu-dropdown_toggle w-dropdown-toggle">
                                <div>free resources</div>
                                <div class="g-nav_menu-dropdown-icon w-icon-dropdown-toggle"></div>
                            </div>
                            <nav class="g-nav_menu-dropdown cc-mobile-nav_menu-dropdown w-dropdown-list">
                                @php
                                    $course_free = DB::table('courses')
                                        ->select('id', 'course_title')
                                        ->where('free', 1)
                                        ->get();
                                @endphp
                                <ul role="list" class="g-nav_menu-dropdown_list">
                                    @foreach ($course_free as $item)
                                        <li class="g-nav_menu-dropdown-list_item"><a
                                                href="{{ route('course_details', $item->id) }}"
                                                class="g-nav_menu-dropdown-list_link w-dropdown-link">
                                                {{ $item->course_title }}</a></li>
                                    @endforeach

                                </ul>
                            </nav>
                        </div>
                    </li>
                    <li class="g-nav_menu-list_item"><a href="{{ route('contact_route') }}"
                            class="g-nav_menu-link_wrapper cc-mobile-nav_menu-link_wrapper w-nav-link">Contact</a>
                    </li>
                </ul>

                @if (auth::check())
                    <div class="g-nav_menu-list cc-cta_button">
                        <a href="{{route('admin.dashboard')}}" class="button-df w-button">Profile</a>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                           <button style="margin-left: 28px" type="submit" class="button-df w-button">Logout</a>
                        </form>

                    </div>





                @else
                    <div class="g-nav_menu-list cc-cta_button"><a data-w-id="1b86f29e-c341-a3c3-3453-969d88d36c0e"
                            href="#" class="button-df w-button">Sign In Here</a>
                    </div>
                @endif



            </div>
        </nav>
        <div class="g-nav_menu-button w-nav-button">
            <div class="g-nav_menu-button_icon w-icon-nav-menu"></div>
        </div>
    </div>
    <div class="g-nav-custom-css w-embed">
        <style>
            /* set 1.6rem = 16px and considers user's browser font size setting */
            html {
                font-size: 62.5%;
            }

            /* turn nav transparent from adding body tag */
            .transparent-nav .g-nav {
                color: #f1f1f1;
                background-color: transparent;
            }

            /* allow mobile nav menu to scroll and darkens overlay */
            @media only screen and (max-width: 991px) {
                .g-nav_menu {
                    height: 100vh !important;
                }
            }

            /* to prevent the default color styling from webflow */
            .g-nav_menu-list_item .w-dropdown-toggle,
            .g-nav_menu-list_item .w-nav-link {
                color: inherit;
            }

            /* rotate caret 180 degrees in mobile menu when open */
            .g-nav_menu-dropdown_wrapper .w--open .g-nav_menu-dropdown-icon {
                transform: rotate(180deg);
            }
        </style>
    </div>
</div>
