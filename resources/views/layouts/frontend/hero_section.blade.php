<div data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2a7" class="section cc-hero cc-home-2">
    <div class="container cc-full w-container">
        <div class="full_width">
            <h1 data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2ab" style="opacity:0" class="hero-title">Expert
                Automation Team</h1>
            <h1 data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2ad" style="opacity:0" class="hero-sub-title mb-40">#1
                IT Institute to Help You Start a Career In TecH.</h1><a href="#trending-courses"
                data-w-id="efe56c2f-cb72-99ed-90b1-9c325bf5c3b8" style="opacity:0" class="button-df cc-large">Start
                Learning Now!</a>
        </div>
    </div>
</div>
