<!DOCTYPE html>
<html data-wf-domain="expertautomationteam.webflow.io" data-wf-page="5d58db2bdc65b87dd3ff4c38"
    data-wf-site="5d58db2bdc65b81d7aff4c37">
    @include('layouts.frontend.header')
    <body>
        @if (session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                text: '{{ session('success') }}',
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                icon: 'error',
                text: '{{ session('error') }}',
            });
        </script>
    @endif
        @include('layouts.frontend.top_bar')
        @include('layouts.frontend.navbar')
        @yield('content')
        @include('layouts.frontend.footer')
    </body>
</html>
