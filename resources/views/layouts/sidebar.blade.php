<style>
    .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active,
    .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
        background-color: rgba(255, 255, 255, .1);
        color: #fff;
    }
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dashboard/img/logo.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin.dashboard') }}" class="d-block">{{ auth()->user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @if(auth()->user()->role_id==1)
                <li class="nav-item menu-open">
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.dashboard') }}"
                                class="nav-link {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dashboard </p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                <li
                    class="nav-item {{ request()->routeIs('roles.index', 'roles.create', 'permission_list') ? 'menu-open' : '' }}">

                    @if (sidebarMenuChange('roles.index') || sidebarMenuChange('roles.create'))
                        <a href="#"
                            class="nav-link {{ request()->routeIs('roles.index', 'roles.create') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Role Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif

                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('roles.index'))
                            <li class="nav-item">
                                <a href="{{ route('roles.index') }}"
                                    class="nav-link {{ request()->routeIs('roles.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('roles.create'))
                            <li class="nav-item">
                                <a href="{{ route('roles.create') }}"
                                    class="nav-link {{ request()->routeIs('roles.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('permission_list'))
                            <li class="nav-item">
                                <a href="{{ route('permission_list') }}"
                                    class="nav-link {{ request()->routeIs('permission_list') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Permission List</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIs('manage-user.index', 'manage-user.create', 'import_user','get_teacher','get_student') ? 'menu-open' : '' }}">

                    @if (sidebarMenuChange('manage-user.index') || sidebarMenuChange('manage-user.create'))
                        <a href="#"
                            class="nav-link {{ request()->routeIs('manage-user.index', 'manage-user.create', 'import_user','get_teacher','get_student') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                User Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif

                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('manage-user.index'))
                            <li class="nav-item">
                                <a href="{{ route('manage-user.index') }}"
                                    class="nav-link {{ request()->routeIs('manage-user.index','get_teacher','get_student') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('manage-user.create'))
                            <li class="nav-item">
                                <a href="{{ route('manage-user.create') }}"
                                    class="nav-link {{ request()->routeIs('manage-user.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('import_user'))
                            <li class="nav-item">
                                <a href="{{ route('import_user') }}"
                                    class="nav-link {{ request()->routeIs('import_user') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Import</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>



                <li
                    class="nav-item {{ request()->routeIs('course-category.index', 'course-category.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('course-category.index') || sidebarMenuChange('course-category.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Course category
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('course-category.index'))
                            <li class="nav-item">
                                <a href="{{ route('course-category.index') }}"
                                    class="nav-link {{ request()->routeIs('course-category.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('course-category.create'))
                            <li class="nav-item">
                                <a href="{{ route('course-category.create') }}"
                                    class="nav-link {{ request()->routeIs('course-category.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>


                <li
                    class="nav-item {{ request()->routeIs('course.index', 'course.create', 'course-enroll.index') ? 'menu-open' : '' }}">

                    @if (sidebarMenuChange('course.index') || sidebarMenuChange('course.create') || sidebarMenuChange('course-enroll.index'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Course Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('course.index'))
                            <li class="nav-item">
                                <a href="{{ route('course.index') }}"
                                    class="nav-link {{ request()->routeIs('course.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('course.create'))
                            <li class="nav-item">
                                <a href="{{ route('course.create') }}"
                                    class="nav-link {{ request()->routeIs('course.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('course-enroll.index'))
                            <li class="nav-item">
                                <a href="{{ route('course-enroll.index') }}"
                                    class="nav-link {{ request()->routeIs('course-enroll.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Student Course Enroll</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIs('student-payment.index', 'student-payment.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('student-payment.index') || sidebarMenuChange('student-payment.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Student Payment
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif

                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('student-payment.index'))
                            <li class="nav-item">
                                <a href="{{ route('student-payment.index') }}"
                                    class="nav-link {{ request()->routeIs('student-payment.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('student-payment.create'))
                            <li class="nav-item">
                                <a href="{{ route('student-payment.create') }}"
                                    class="nav-link {{ request()->routeIs('student-payment.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>


                <li
                    class="nav-item {{ request()->routeIs('exam.index', 'exam.create', 'get_certificate') ? 'menu-open' : '' }}">

                    @if (sidebarMenuChange('exam.index') || sidebarMenuChange('exam.create') || sidebarMenuChange('get_certificate'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Exam
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('exam.index'))
                            <li class="nav-item">
                                <a href="{{ route('exam.index') }}"
                                    class="nav-link {{ request()->routeIs('exam.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('exam.create'))
                            <li class="nav-item">
                                <a href="{{ route('exam.create') }}"
                                    class="nav-link {{ request()->routeIs('exam.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('get_certificate'))
                            <li class="nav-item">
                                <a href="{{ route('get_certificate') }}"
                                    class="nav-link {{ request()->routeIs('get_certificate') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Exam Result</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIs('quiz.index', 'quiz.create', 'get_quiz') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('quiz.index') || sidebarMenuChange('quiz.create') || sidebarMenuChange('get_quiz'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Quiz
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('quiz.index'))
                            <li class="nav-item">
                                <a href="{{ route('quiz.index') }}"
                                    class="nav-link {{ request()->routeIs('quiz.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('quiz.create'))
                            <li class="nav-item">
                                <a href="{{ route('quiz.create') }}"
                                    class="nav-link {{ request()->routeIs('quiz.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('get_quiz'))
                            <li class="nav-item">
                                <a href="{{ route('get_quiz') }}"
                                    class="nav-link {{ request()->routeIs('get_quiz') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Quiz Result</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>



                <li
                    class="nav-item {{ request()->routeIs('announcement.index', 'announcement.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('announcement.index') || sidebarMenuChange('announcement.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Announcement
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('announcement.index'))
                            <li class="nav-item">
                                <a href="{{ route('announcement.index') }}"
                                    class="nav-link {{ request()->routeIs('announcement.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('announcement.create'))
                            <li class="nav-item">
                                <a href="{{ route('announcement.create') }}"
                                    class="nav-link {{ request()->routeIs('announcement.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                <li
                class="nav-item {{ request()->routeIs('notice.index', 'notice.create') ? 'menu-open' : '' }}">
                @if (sidebarMenuChange('notice.index') || sidebarMenuChange('notice.create'))
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Course Notice
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                @endif
                <ul class="nav nav-treeview">
                    @if (sidebarMenuChange('notice.index'))
                        <li class="nav-item">
                            <a href="{{ route('notice.index') }}"
                                class="nav-link {{ request()->routeIs('notice.index') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List</p>
                            </a>
                        </li>
                    @endif
                    @if (sidebarMenuChange('notice.create'))
                        <li class="nav-item">
                            <a href="{{ route('notice.create') }}"
                                class="nav-link {{ request()->routeIs('notice.create') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add</p>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>



                <li class="nav-item {{ request()->routeIs('video.index', 'video.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('video.index') || sidebarMenuChange('video.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Video Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('video.index'))
                            <li class="nav-item">
                                <a href="{{ route('video.index') }}"
                                    class="nav-link {{ request()->routeIs('video.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('video.create'))
                            <li class="nav-item">
                                <a href="{{ route('video.create') }}"
                                    class="nav-link {{ request()->routeIs('video.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>



                <li
                    class="nav-item {{ request()->routeIs('record-video.index', 'record-video.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('record-video.index') || sidebarMenuChange('record-video.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Recorded Video
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('record-video.index'))
                            <li class="nav-item">
                                <a href="{{ route('record-video.index') }}"
                                    class="nav-link {{ request()->routeIs('record-video.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('record-video.create'))
                            <li class="nav-item">
                                <a href="{{ route('record-video.create') }}"
                                    class="nav-link {{ request()->routeIs('record-video.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>



                <li class="nav-item {{ request()->routeIs('company.index', 'company.create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('company.index') || sidebarMenuChange('company.create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Company Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('company.index'))
                            <li class="nav-item">
                                <a href="{{ route('company.index') }}"
                                    class="nav-link {{ request()->routeIs('company.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('company.create'))
                            <li class="nav-item">
                                <a href="{{ route('company.create') }}"
                                    class="nav-link {{ request()->routeIs('company.create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>




                <li class="nav-item {{ request()->routeIs('promo_index', 'promo_create') ? 'menu-open' : '' }}">
                    @if (sidebarMenuChange('promo_index') || sidebarMenuChange('promo_create'))
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Promocode
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                    @endif
                    <ul class="nav nav-treeview">
                        @if (sidebarMenuChange('promo_index'))
                            <li class="nav-item">
                                <a href="{{ route('promo_index') }}"
                                    class="nav-link {{ request()->routeIs('promo_index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        @endif
                        @if (sidebarMenuChange('promo_create'))
                            <li class="nav-item">
                                <a href="{{ route('promo_create') }}"
                                    class="nav-link {{ request()->routeIs('promo_create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                @if (sidebarMenuChange('send-email.index'))
                    <li class="nav-item {{ request()->routeIs('send-email.index') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Send email
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('send-email.index') }}"
                                    class="nav-link {{ request()->routeIs('send-email.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Send email</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif


                @if (sidebarMenuChange('send-sms.index'))
                    <li class="nav-item {{ request()->routeIs('send-sms.index') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Send sms
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('send-sms.index') }}"
                                    class="nav-link {{ request()->routeIs('send-sms.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Send sms</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (sidebarMenuChange('channel_route') || sidebarMenuChange('channel_route'))
                    <li
                        class="nav-item {{ request()->routeIs('channel_route', 'channel_route_create') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Channel
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if (sidebarMenuChange('channel_route'))
                                <li class="nav-item">
                                    <a href="{{ route('channel_route') }}"
                                        class="nav-link {{ request()->routeIs('channel_route') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>List</p>
                                    </a>
                                </li>
                            @endif
                            @if (sidebarMenuChange('channel_route_create'))
                                <li class="nav-item">
                                    <a href="{{ route('channel_route_create') }}"
                                        class="nav-link {{ request()->routeIs('channel_route_create') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if (sidebarMenuChange('activity-log.index'))
                    <li class="nav-item {{ request()->routeIs('activity-log.index') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Activity Log
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('activity-log.index') }}"
                                    class="nav-link {{ request()->routeIs('activity-log.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Student log</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (sidebarMenuChange('activity-log.index'))
                    <li class="nav-item {{ request()->routeIs('contact-student.index') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Contact Information
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('contact-student.index') }}"
                                    class="nav-link {{ request()->routeIs('contact-student.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                {{-- <li class="nav-item {{ request()->routeIs('meet-instructor.index') ? 'menu-open' : '' }}">
                    <a href="{{ route('meet-instructor.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Student Meet Request
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                </li> --}}
                @if (sidebarMenuChange('s_certificate'))
                <li class="nav-item {{ request()->routeIs('s_certificate') ? 'menu-open' : '' }}">
                    <a href="{{ route('s_certificate') }}" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Generate Certificate
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                </li>
                @endif

                <li class="nav-item {{ request()->routeIs('video_call') ? 'menu-open' : '' }}">
                    <a href="{{ route('video_call') }}" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Join Video Call
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                </li>



                {{-- @if (sidebarMenuChange('activity-log.index'))
                    <li class="nav-item {{ request()->routeIs('system-settings.index') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Settings
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('system-settings.index') }}"
                                    class="nav-link {{ request()->routeIs('system-settings.index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>System Setting</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif --}}

                <li class="nav-item">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"
                        class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Logout
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
