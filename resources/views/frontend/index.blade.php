@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
@include('frontend.announcement_popup')
<div data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2a7" class="section cc-hero cc-home-2">
    <div class="container cc-full w-container">
        <div class="full_width">
            <h1 data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2ab" style="opacity:0" class="hero-title">Expert
                Automation Team</h1>
            <h1 data-w-id="55780d6e-4d02-8d12-6e7c-873f9970e2ad" style="opacity:0" class="hero-sub-title mb-40">#1
                IT Institute to Help You Start a Career In TecH.</h1><a href="#trending-courses"
                data-w-id="efe56c2f-cb72-99ed-90b1-9c325bf5c3b8" style="opacity:0" class="button-df cc-large">Start
                Learning Now!</a>
        </div>
    </div>
</div>

    <!-- /* our works section */ -->
    <div class="flex-space-between wf-section">
        <div class="flex-width-1-4 cc-full cc-home-card bg-orange">
            <div class="home-card-icon"><span class="icon-font cc-home-card"></span></div>
            <h1 class="heading-h1 text-nv">500+</h1>
            <h4 class="heading-h4 text-white text-center">Students took the courses</h4>
        </div>
        <div class="flex-width-1-4 cc-full cc-home-card bg-nv">
            <div class="home-card-icon"><span class="icon-font cc-home-card"></span></div>
            <h1 class="heading-h1 text-orange">2500+</h1>
            <h4 class="heading-h4 text-white text-center">Projects completed</h4>
        </div>
        <div class="flex-width-1-4 cc-full cc-home-card bg-orange">
            <div class="home-card-icon"><span class="icon-font cc-home-card">$</span></div>
            <h1 class="heading-h1 text-nv">100%</h1>
            <h4 class="heading-h4 text-white text-center">Job Placement Assistance</h4>
        </div>
        <div class="flex-width-1-4 cc-full cc-home-card bg-nv">
            <div class="home-card-icon"><span class="icon-font cc-home-card"></span></div>
            <h1 class="heading-h1 text-orange">500+</h1>
            <h4 class="heading-h4 text-white text-center">Happy Clients with Jobs</h4>
        </div>
    </div>

    <!-- /* Trending Courses section */ -->
    <div class="section wf-section">
        <div class="container w-container">
            <div class="flex-space-between">
                <div data-w-id="71d39c3a-220e-0d04-5d3e-d5d9732b0c89" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title">Trending Courses</h2>
                    <div class="divider-full cc-small cc-section-title mb-20"></div>
                </div>

                @foreach ($data as $item)
                    <div data-w-id="71d39c3a-220e-0d04-5d3e-d5d9732b0c8d" style="opacity:0"
                        class="flex-width-1-3 cc-tr-courses cc-with-bg">
                        <div class="card-sm"><img src="{{ asset('dashboard/course/' . $item->course_image) }}"
                                loading="lazy"
                                sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 90vw, 26vw"
                                srcset="{{ asset('dashboard/course/' . $item->course_image) }}" alt="course image"
                                class="image-wrapper border-radius mb-20" /><a
                                href="{{ route('course_details', $item->id) }}" class="button-df cc-enroll">Enroll Now</a>
                        </div><a href="{{ route('course_details', $item->id) }}"
                            class="button-df cc-enroll bg-nv">{{ $item->course_title }}</a>
                    </div>
                @endforeach

                <div data-w-id="71d39c3a-220e-0d04-5d3e-d5d9732b0cba" style="opacity:0"
                    class="full_width text-center flex-center"><a href="{{ route('all_course_route') }}"
                        class="button-df cc-large bg-df">All Courses</a></div>
            </div>
        </div>
    </div>

    <!-- /* Take action TODAY section */ -->
    <div class="section cc-register-now wf-section">
        <div class="container w-container">
            <div class="full_width">
                <h1 class="paragraph cc-large text-white text-center">With a <strong>proven course curriculum</strong>,
                    we&#x27;ve helped over 500 students to skip the rat race, work in a low-paying or a minimum wage job,
                    and
                    jump-start a career in tech, specifically as a <strong>QA Automation Engineer</strong> where the salary
                    range
                    is between <strong>$80k to $120k annually</strong>.Classes are available both <strong>Online</strong>
                    and
                    <strong>Onsite</strong>.
                </h1>
            </div>
            <div class="full_width mt-20">
                <h3 class="heading-h3 text-white text-center mb-20"><strong>Take action TODAY and join the Six Figure
                        Club.</strong></h3>
            </div>
            <div data-w-id="fed42339-b322-01d7-15d3-b880519604e1" style="opacity:0"
                class="full_width text-center flex-center mt-40"><a href="#" class="button-df cc-large bg-white">Start
                    Learning
                    Now!</a></div>
        </div>
    </div>

    <!-- /* Free Videos section */ -->
    <div class="section cc-video wf-section">
        <div class="container w-container">
            <div class="flex-space-between">
                <div data-w-id="761ca797-5198-f049-b0e3-b5d12c3a722f" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title">Free Videos</h2>
                    <div class="divider-full cc-small cc-section-title mb-20"></div>
                </div>
                @foreach ($free_video as $free_videos)
                <div class="flex-width-1-3 cc-tr-courses">
                    <div style="padding-top:56.17021276595745%" class="w-embed-youtubevideo">
                      <iframe src="{{ $free_videos->url }}" frameborder="0" style="position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:auto" allow="autoplay; encrypted-media" allowfullscreen="" title="Quality Assurance Engineering Course Details"></iframe>
                    </div>
                    <div class="card-sm cc-border cc-warning">
                      <h4 class="heading-h5 text-primary">{{ date('d F Y H:i:s a', strtotime($free_videos->publish_date)) }}</h4>
                    </div>
                  </div>
                @endforeach


                <div data-w-id="c848ff8c-93d0-dd38-169a-e2ad96e33fdb" style="opacity:0" class="full_width text-center">
                    <h2 class="heading-h2 cc-section-title cc-small">Latest youtube videos</h2>
                </div>

                @foreach ($recorded_video as $recorded_video)
                <div class="flex-width-1-3 cc-tr-courses">
                    <div style="padding-top:56.17021276595745%" class="w-embed-youtubevideo">
                      <iframe src="{{ $recorded_video->url }}" frameborder="0" style="position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:auto" allow="autoplay; encrypted-media" allowfullscreen="" title="Quality Assurance Engineering Course Details"></iframe>
                    </div>
                    <div class="card-sm cc-border cc-warning">
                      <h4 class="heading-h5 text-primary">{{$recorded_video->title}}</h4>
                    </div>
                  </div>
                @endforeach

                <div data-w-id="761ca797-5198-f049-b0e3-b5d12c3a7251" style="opacity:0"
                    class="full_width text-center flex-center"><a href="{{route('all_video_route')}}" class="button-df cc-large bg-df">More
                        Videos</a></div>
            </div>
        </div>
    </div>

{{-- company logo --}}
    <div class="divider-full"></div>
    <div class="section wf-section">
        <div class="container w-container">
            <div data-w-id="547e4c91-1115-22d2-b72d-e6c73a8f00f1" style="opacity:0"
                class="full_width text-center flex-center">
                <h2 class="heading-h2 cc-section-title">SOME COMPANIES</h2>
                <div class="divider-full cc-small cc-section-title mb-20"></div>
            </div>
            <div data-delay="4000" data-animation="slide" class="slider w-slider" data-autoplay="true"
                data-easing="linear" data-hide-arrows="false" data-disable-swipe="false" data-autoplay-limit="0"
                data-nav-spacing="3" data-duration="500" data-infinite="true">
                <div class="slider-mask w-slider-mask">
                    @foreach($company_logo as $logo)
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <div class="flex-width-1-5 flex-center"><img
                                src="{{asset('dashboard/company/'.$logo->logo)}}"
                                loading="lazy" sizes="(max-width: 479px) 100vw, (max-width: 767px) 28vw, 17vw"
                                srcset="{{asset('dashboard/company/'.$logo->logo)}}"
                                alt="EAT" class="image-wrapper cc-company" /></div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="slider-arrow w-slider-arrow-left">
                    <div class="slider-arrow-icon cc-left w-icon-slider-left"></div>
                </div>
                <div class="slider-arrow w-slider-arrow-right">
                    <div class="slider-arrow-icon cc-right w-icon-slider-right"></div>
                </div>
                <div class="slide-nav w-slider-nav w-slider-nav-invert w-shadow w-round"></div>
            </div>
        </div>
    </div>



    <!-- /* tastimonials section */ -->
    <div class="divider-full"></div>
    <div class="section wf-section">
        <div class="container w-container">
            <div data-w-id="547e4c91-1115-22d2-b72d-e6c73a8f00f1" style="opacity:0"
                class="full_width text-center flex-center">
                <h2 class="heading-h2 cc-section-title">tastimonials</h2>
                <div class="divider-full cc-small cc-section-title mb-20"></div>
            </div>
            <div data-delay="4000" data-animation="slide" class="slider w-slider" data-autoplay="true"
                data-easing="linear" data-hide-arrows="false" data-disable-swipe="false" data-autoplay-limit="0"
                data-nav-spacing="3" data-duration="500" data-infinite="true">
                <div class="slider-mask w-slider-mask">
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                    <div class="slide w-slide">
                        <div class="flex-center cc-testimonials">
                            <h5 class="testimonial-icon bg-orange"><span class="icon-font"></span></h5>
                            <h5 class="heading-h5 text-nv text-center cc-normal">We focus on getting you job-ready. We
                                teach exactly
                                what is needed and do not waste your time.I got 2 offers even before I finished the course.
                                Woooo
                                hooooooo.</h5>
                        </div>
                    </div>
                </div>
                <div class="slider-arrow w-slider-arrow-left">
                    <div class="slider-arrow-icon cc-left w-icon-slider-left"></div>
                </div>
                <div class="slider-arrow w-slider-arrow-right">
                    <div class="slider-arrow-icon cc-right w-icon-slider-right"></div>
                </div>
                <div class="slide-nav w-slider-nav w-slider-nav-invert w-shadow w-round"></div>
            </div>
        </div>
    </div>


    <!-- /* Test & quiz page view section */ -->
    <div class="divider-full"></div>
    {{-- <div id="about-us" class="section wf-section">
        <div class="container w-container">
            <div data-w-id="94572a31-a6cd-a047-0211-0981819ed6de" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">Test &amp; quiz page view</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="flex-space-between">
                <div class="full_width">
                    <div class="w-form">
                        <form id="email-form-2" name="email-form-2" data-name="Email Form 2" method="get"><label
                                for="question" class="paragraph cc-question-title">1. What is the full form of
                                HTML?</label><label class="w-checkbox paragraph cc-question-options"><input
                                    type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" required=""
                                    class="w-checkbox-input" /><span class="w-form-label" for="checkbox">Multiple
                                    answer</span></label><label class="w-checkbox paragraph cc-question-options"><input
                                    type="checkbox" id="checkbox-5" name="checkbox-5" data-name="Checkbox 5"
                                    required="" class="w-checkbox-input" /><span class="w-form-label"
                                    for="checkbox-5">Multiple answer</span></label><label
                                class="w-checkbox paragraph cc-question-options"><input type="checkbox" id="checkbox-4"
                                    name="checkbox-4" data-name="Checkbox 4" required=""
                                    class="w-checkbox-input" /><span class="w-form-label" for="checkbox-4">Multiple
                                    answer</span></label><label for="email" class="paragraph cc-question-title">2. What
                                is the full form of CSS?</label><label class="paragraph cc-question-options w-radio"><input
                                    type="radio" data-name="Radio" id="radio" name="radio" value="Radio"
                                    required="" class="w-form-formradioinput w-radio-input" /><span
                                    class="w-form-label" for="radio">Single answer</span></label><label
                                class="paragraph cc-question-options w-radio"><input type="radio" data-name="Radio 4"
                                    id="radio-4" name="radio" value="Radio" required=""
                                    class="w-form-formradioinput w-radio-input" /><span class="w-form-label"
                                    for="radio-4">Single answer</span></label><label
                                class="paragraph cc-question-options w-radio"><input type="radio" data-name="Radio 3"
                                    id="radio-3" name="radio" value="Radio" required=""
                                    class="w-form-formradioinput w-radio-input" /><span class="w-form-label"
                                    for="radio-3">Single answer</span></label><input type="submit" value="Submit"
                                data-wait="Please wait..." class="button-df mt-20 w-button" /></form>
                        <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- /* course package page view section */ -->
    {{-- <div id="about-us" class="section wf-section">
        <div class="container w-container">
            <div data-w-id="1f2f80b4-995d-8cf9-e64f-994e2a26be03" style="opacity:0"
                class="full_width text-center flex-center mb-40">
                <h2 class="heading-h2 cc-section-title text-nv">course package page view</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="flex-space-between">
                <div class="flex-width-1-3 cc-packages">
                    <h3 class="heading-h4 cc-course-title">Manual Testing (Beginner)</h3>
                    <ul role="list" class="list cc-course-features">
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                    </ul><a href="#" class="button-df bg-df mt-20">Explore More</a>
                </div>
                <div class="flex-width-1-3 cc-packages">
                    <h3 class="heading-h4 cc-course-title">Manual Testing (Intermediate)</h3>
                    <ul role="list" class="list cc-course-features">
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                    </ul><a href="#" class="button-df bg-df mt-20">Enroll Now</a>
                </div>
                <div class="flex-width-1-3 cc-packages">
                    <h3 class="heading-h4 cc-course-title">Manual Testing (Expert)</h3>
                    <ul role="list" class="list cc-course-features">
                        <li class="list-item"><strong>Lorem Ipsum</strong> is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                        <li class="list-item">Lorem Ipsum is simply dummy text</li>
                    </ul><a href="#" class="button-df bg-df mt-20">Subscribe</a>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
