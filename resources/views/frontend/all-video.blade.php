@extends('layouts.app_front')
@section('title')
@endsection
@section('content')

    <!-- /* Free Videos section */ -->
    <div class="section cc-video wf-section">
        <div class="container w-container">
            <div class="flex-space-between">
                <div data-w-id="761ca797-5198-f049-b0e3-b5d12c3a722f" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title">Free Videos</h2>
                    <div class="divider-full cc-small cc-section-title mb-20"></div>
                </div>
                @foreach ($free_video as $free_videos)
                    <div class="flex-width-1-3 cc-free-vdo">
                        <div style="padding-top:56.17021276595745%" class="w-embed-youtubevideo">
                            <iframe style="position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:auto"
                                src="{{ $free_videos->url }}" title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write;
               encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowfullscreen>
                            </iframe>
                        </div>
                        <div class="card-sm cc-border cc-warning">
                            <h4 class="heading-h5 text-danger">Will Live On
                                {{ date('d F Y H:i:s a', strtotime($free_videos->publish_date)) }}</h4>
                        </div>
                    </div>
                @endforeach


                <div data-w-id="c848ff8c-93d0-dd38-169a-e2ad96e33fdb" style="opacity:0" class="full_width text-center">
                    <h2 class="heading-h2 cc-section-title cc-small">Latest youtube videos</h2>
                </div>

                @foreach ($recorded_video as $recorded_video)
                    <div class="flex-width-1-3">
                        <div style="padding-top:56.17021276595745%" class="w-embed-youtubevideo">
                            <iframe src="{{ $recorded_video->url }}" frameBorder="0"
                                style="position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:auto"
                                allow="autoplay; encrypted-media" allowfullscreen=""
                                title="Do you want to skip the rate race and make $80k to $120k annually? Get in touch with us."></iframe>
                        </div>
                        <div class="card-sm cc-border cc-warning">
                            <h4 class="heading-h5 text-primary">Recorded</h4>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
