@extends('layouts.app_front')
@section('title')  @endsection
@section('content')

<div id="about-us" class="section mt-100">
    <div class="container w-container">
        <div class="flex-space-between">
            <div data-w-id="02eb6fd0-5746-5cfc-acc8-a5d2d06ee8e0" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">About Us</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="full_width text-center">
                <div class="paragraph cc-large mb-20">With a <strong>proven course curriculum</strong>, we&#x27;ve
                    helped over 500 students to skip the rat race, work in a low-paying or a minimum wage job, and
                    jump-start a career in tech, specifically as a <strong>QA Automation Engineer</strong> where the
                    salary range is between <strong>$80k to $120k annually</strong>.<br />Classes are available both
                    <strong>Online</strong> and <strong>Onsite</strong>.<br />‍<strong>Take action TODAY and join
                        the Six Figure Club.</strong></div><a href="#" class="button-df">Contact Us</a>
            </div>
            <div class="full_width"><img
                    src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2.png"
                    loading="lazy" sizes="(max-width: 479px) 87vw, (max-width: 767px) 92vw, 94vw"
                    srcset="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2-p-500.png 500w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2-p-800.png 800w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2-p-1080.png 1080w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2-p-1600.png 1600w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644126fd79935b3ad8a343cc_justLikeRealJob_v2.png 1767w"
                    alt="" class="image-wrapper" /></div>
            <div class="flex-width-1-2 border-df">
                <div class="paragraph cc-large text-center"><strong>No Fluffs.</strong><br />We focus on getting you
                    job-ready. We teach exactly what is needed and do not waste your time.<br /><br /><strong>No IT
                        experience, No problem.<br />We teach you everything.</strong><strong></strong></div>
            </div>
            <div class="flex-width-1-2 border-df">
                <div class="paragraph cc-large text-center"><strong>The best way to learn is Hands-on.</strong>
                    <br />Not only do we teach you how to do it but you will build on 6-10 different projects by the
                    end of the course.<strong></strong></div>
            </div>
            <div class="full_width"><img
                    src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644124b8cf797bc9d6f1b478_jobPlacement.png"
                    loading="lazy" sizes="(max-width: 479px) 87vw, (max-width: 767px) 92vw, 94vw"
                    srcset="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644124b8cf797bc9d6f1b478_jobPlacement-p-500.png 500w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644124b8cf797bc9d6f1b478_jobPlacement-p-800.png 800w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644124b8cf797bc9d6f1b478_jobPlacement-p-1080.png 1080w, https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/644124b8cf797bc9d6f1b478_jobPlacement.png 1616w"
                    alt="" class="image-wrapper" /></div>
        </div>
    </div>
</div>
@endsection
