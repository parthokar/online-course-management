@php
    $announcement = DB::table('announcements')
        ->where('start_date', date('Y-m-d'))
        ->where('end_date', '>',date('Y-m-d'))
        ->orderBy('id','DESC')
        ->first();
@endphp
@if (isset($announcement))
    <div data-w-id="ec5afb26-b9bc-741f-e281-1c35a4393e2a" class="announcement-wrapper" style="display: none;">
        <div data-w-id="6bf93ea7-ae99-59ac-08d7-adebb2837055" class="announcement-content" style="opacity: 0;">
            <div data-w-id="cdd15683-2af4-3641-2661-9c60ebadc989" class="announcement-close"></div>
            <div class="full_width text-center">
                <h1 class="text-upper">{{ $announcement->topics }}</h1>
                <div class="paragraph">{!! $announcement->details !!}
                    <br>Date:
                    {{ date('d-F-Y', strtotime($announcement->start_date)) }}
                     to
                     {{ date('d-F-Y', strtotime($announcement->end_date)) }}
                </div>
            </div>
        </div>
    </div>
@endif
