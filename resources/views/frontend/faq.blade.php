@extends('layouts.app_front')
@section('title')  @endsection
@section('content')
<div id="faq" class="section mt-100">
    `    <div class="container w-container">
            <div data-w-id="1dccc215-02e6-7095-b6cc-3d56a78c3637" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">FAQ</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="flex-space-between">
                <div class="full_width">
                    <h3 class="text-lg">What is QA Engineering?</h3>
                    <div class="paragraph">QA is Quality Assurance Engineering. As a QA, our job is to make sure the
                        quality of the application meets the requirements. We do this by testing the application fully
                        and try to break the system by testing not only in a conventional way but also following
                        unconventional and complex scenarios.</div>
                    <h3 class="text-lg">If I do not have any technical background, can I still take this course?</h3>
                    <div class="paragraph">You do not need to have technical background, but you do need to have basic
                        computer skills, be able to communicate and understand English, and have US work authorization
                        if you need job assistance.Everything else that is required technically we will teach you.</div>
                    <h3 class="text-lg">What makes you different than others?</h3>
                    <div class="paragraph">All of our instructors currently work in the industry and you learn what they
                        do at the job on a regular basis. We skip the bulls**t materials and teach you what you actually
                        need for the interview and for the job. We focus more on hands-on, so you actually know what to
                        do and how to do it. You are assigned hands-on projects, tasks, and homework’s, so you learn the
                        materials more and no room for errors or not knowing how to do something.</div>
                    <h3 class="text-lg">Can I pay in installments?</h3>
                    <div class="paragraph">Yes, you can pay in installments.</div>
                    <h3 class="text-lg">How long are the Live Instructor-Led courses and what days?</h3>
                    <div class="paragraph">Course duration is 12 weeks, twice per week. You can take it on the Weekend
                        (Saturday &amp; Sunday) or on the Weekdays (Monday and Tuesday). These are Live Instructor Led
                        courses available both Onsite and Online. All sessions also get recorded and uploaded after the
                        session is done for the day.</div>
                    <h3 class="text-lg">What is the average time for a student to land a job and how much salary can I
                        expect?</h3>
                    <div class="paragraph">On average, it usually takes 4 to 8 weeks after going to the market for our
                        students to land a job and our students can expect to make $80k to $120k annually.</div>
                    <h3 class="text-lg">What is the difference between Live Instructor-Led Online and Onsite vs.
                        Self-Paced?</h3>
                    <div class="paragraph">Live Instructor-Led Online and Onsite aretaught by aninstructor. Students
                        will follow a specific curriculum based on the batch they registerin. They will be able to
                        interact with the Instructor and the Students Live and ask questions.<br /><br />Self-Paced are
                        for those who wants to learn based on their own schedule. <br /><br />Whether you register for
                        the Live or the Self-Paced class, there will be projects and exams that you must complete. Lots
                        of Hands-On materials are integrated for you to learn from and get more hands-on practice. If
                        you have any questions, you will be able to ask us so we can help. You can also schedule a time
                        with an instructor should you need additional help. After the course, we will provide you Job
                        Assistance if you need it.</div>
                </div>
            </div>
        </div>
    </div>`
@endsection
