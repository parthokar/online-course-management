@extends('layouts.app_front')
@section('title')  @endsection
@section('content')
<div id="faq" class="section mt-100">
        <div class="container w-container">
            <div class="flex-space-between">
                <div class="full_width">



                    <div id="reset-password" class="section">
                        <div class="container w-container">
                          <div class="flex-space-between">
                            <div data-w-id="a745b1e7-52b7-3cb2-34ec-52b1864df869" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;" class="full_width text-center flex-center mb-20">
                              <h2 class="heading-h2 cc-section-title text-nv">Reset password</h2>
                              <div class="divider-full cc-small cc-section-title bg-nv"></div>
                            </div>
                            <div class="full_width">
                              <div class="w-form">
                                <form action="{{ route('u_r_p_post') }}" id="email-form" name="email-form" data-name="Email Form" method="post" data-wf-page-id="5d58db2bdc65b87dd3ff4c38" data-wf-element-id="a745b1e7-52b7-3cb2-34ec-52b1864df86f" aria-label="Email Form">
                                    @csrf
                                  <div class="flex-vr-center">
                                    <div class="flex-width-12">
                                      <label for="student_email-2">Registered Email</label>
                                      <input type="email" class="w-input" maxlength="256" name="email" value="{{old('email')}}" data-name="Student Email 2" placeholder="Enter your registered email here" id="student_email-2" required="">
                                    </div>
                                    <input type="submit" value="Send Request" data-wait="Please wait..." class="button-df mt-20 w-button">
                                  </div>
                                </form>
                                <div class="w-form-done" tabindex="-1" role="region" aria-label="Email Form success">
                                  <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail" tabindex="-1" role="region" aria-label="Email Form failure">
                                  <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>












                </div>
            </div>
        </div>
    </div>
@endsection
