@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<style>
    /* CSS for the chat box container */
    .chat-box {
        list-style: none;
        padding: 0;
        margin: 0;
        display: flex;
        flex-direction: column;
    }

    /* CSS for individual chat messages */
    .chat-box li {
        padding: 8px;
        margin: 8px 0;
        max-width: 70%;
        border-radius: 8px;
        text-align: center;
    }

    /* Your messages (You) */
    .chat-box li.you {
        background-color: blueviolet;
        color: white;
        align-self: flex-end;
    }

    /* Other messages */
    .chat-box li.other {
        background-color: purple;
        color: white;
        align-self: flex-start;
    }

    /* CSS for chat message container */
    .chat-container {
        position: relative;
    }

    /* CSS for the textarea container */
    .textarea-container {
        position: relative;
    }

    /* Adjust padding for the message input to leave space for the button */
    #chat_message {
        padding-right: 80px;
    }

    /* CSS for the send button */
    .btn-primary {
        position: absolute;
        top: 0;
        right: 0;
        height: 100%;
        width: 80px; /* Adjust the button width as needed */
        border-radius: 0 8px 8px 0; /* Rounded corner on the right side */
    }
    </style>
    <section id="after-student-login" class="section mt-100">
        <div class="w-layout-blockcontainer container w-container">
            <div class="flex-space-between">
                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">{{$course->course_title}}</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>
                <input type="hidden" value="{{ $id }}" id="id">
                <div class="messaging">
                            <div id="chat_message_history" class="chat-box" style="background: #e7e7e7;

                            height: 300px;
                            overflow: scroll"></div>

                            <input type="hidden" id="slack_id" value="{{ $id }}">
                            <input type="hidden" id="login_id" value="{{ auth()->user()->id }}">
                            <div class="form-group mt-5 chat-container">
                                <div class="textarea-container">
                                    <textarea style="width: 1000px" id="chat_message" class="form-control" name="message"></textarea>
                                </div>
                                <button onclick="messageList({{ $id }},{{ auth()->user()->id }})" type="button" class="btn btn-primary btn-sm">Send</button>
                            </div>
                </div>
            </div>
        </div>
    </section>

    @section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        function messageList(id, authId) {
            var message = $("#chat_message").val();
            if (message == '') {
                alert('enter message');
                return false;
            }
            $("#reload_message").hide();
            axios.post('{{ route('a_message_route_store') }}', {
                    slack_id: id,
                    message: message,
                })
                .then(function(response) {
                    message.value='';
                    $("#reload_message").hide();
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

        $(document).ready(function() {
            var parameterValue = $("#slack_id").val();
            var parameterValue2 = $("#login_id").val();
            setInterval(function() {
                messageData(parameterValue, parameterValue2);
            }, 2000);
        });


        function messageData(id, authId) {
    axios.get('{{ url('ajax/') }}' + "/" + id, {})
        .then(function(response) {
            var options = '';
            $.each(response.data, function(index, item) {

                var responseDateTime = item.created_at;

                var date = new Date(responseDateTime);

                var formattedDateTime = date.toLocaleString('en-BD', {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true
                });

                var message_info;
                if (item.user_id == authId) {
                    message_info = '<li class="you">You: '   + item.message +  "<br>" + formattedDateTime + '</li>';
                } else {
                    message_info = '<li class="other">' + item.user.name + ': ' + item.message + "<br>" + formattedDateTime + '</li>';
                }
                options += message_info;
            });
            $('#chat_message_history').html(options);
        })
        .catch(function(error) {
            console.log(error);
        });
}

    </script>
@endsection
@endsection
