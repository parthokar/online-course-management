@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<section id="after-student-login-selected-course" class="section mt-100">
    <div class="w-layout-blockcontainer container w-container">
        <div class="flex-space-between">
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977ec" style="opacity:0"
                class="full_width">
                <h2 class="heading-h2 cc-section-title text-nv">Quiz</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977fa" style="opacity:0"
                class="full_width">
                <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977fa" style="opacity:0"
                class="flex-space-between">
                @if($quiz->count()>0)
               @foreach($quiz as $quizs)
                <div class="flex-width-1-3 flex-vr-center">
                    <a href="{{route('c_q',array('course_id'=>$course->id,'quiz_id'=>$quizs->id))}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">{{$quizs->title}}</h4>
                    </a>
                </div>
               @endforeach
               @else
               No data found
               @endif
                </div>
            </div>
        </div>
        <div class="flex-space-between">
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977ec" style="opacity:0"
                class="full_width">
                <h2 class="heading-h2 cc-section-title text-nv">Test</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977fa" style="opacity:0"
                class="full_width">
                <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977fa" style="opacity:0"
                class="flex-space-between">
                @if($exam->count()>0)
                @foreach($exam as $exams)
                <div class="flex-width-1-3 flex-vr-center">
                    <a href="{{route('c_t',array('course_id'=>$course->id,'test_id'=>$exams->id))}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">{{$exams->title}}</h4>
                    </a>
                </div>
                @endforeach
                @else
                No data found
                @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
