@extends('layouts.app_front')
@section('title')
@endsection

@section('content')
<section id="after-student-login" class="section mt-100">
    <div class="w-layout-blockcontainer container w-container">
      <div class="flex-space-between">
        <div data-w-id="e2142134-c1a0-9067-0a03-760ddd122213" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;" class="full_width text-center flex-center">
          <h2 class="heading-h2 cc-section-title text-nv">Course notice</h2>
          <div class="divider-full cc-small cc-section-title bg-nv"></div>
        </div>

        @if($notice->count()>0)
        @foreach($notice as $notices)
          <div class="full_width card-sm cc-notice">
            <h4 class="heading-h5 cc-notice mb-20">{{$notices->topic}}</h4>
            <p>
                 Datetime: @php $notice_date = date('Y-m-d',strtotime($notices->date_time)); @endphp @if($notice_date==date('Y-m-d')) Today @else {{$notices->date_time}} @endif
            </p>
            <div class="paragraph cc-md">{!! $notices->description !!}</div>
          </div>
          @endforeach
          @else
          No Notice Found
          @endif
      </div>
    </div>
  </section>
@endsection



