@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <section id="after-student-login" class="section mt-100">

        <div class="w-layout-blockcontainer container w-container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="flex-space-between">
                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">{{ $course->course_title }}</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>

                <h4>Course Teacher: {{ $instructor->name }} <br> Email: {{ $instructor->email }}</h4>

                <form action="{{ route('meet-instructor.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                            <div class="form-group">
                                <strong>Select Time:</strong>
                                <input type="datetime-local" name="request_time" class="form-control" required>
                            </div>
                        </div>
                        <input type="hidden" name="teacher_id" value="{{ $instructor->id }}">
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                            <button type="submit" class="btn btn-primary" style="font-size: 14px">Send Meet
                                Request</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </section>
@endsection
