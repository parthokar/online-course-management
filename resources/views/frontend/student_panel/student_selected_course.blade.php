@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<section id="after-student-login-selected-course" class="section mt-100">
    <div class="w-layout-blockcontainer container w-container">
        <div class="flex-space-between">
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977ec" style="opacity:0"
                class="full_width text-center flex-center">
                <h2 class="heading-h2 cc-section-title text-nv">{{$course->course_title}}</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977f0" style="opacity:0"
                class="flex-width-1-3 flex-vr-center"><a href="{{$course->live_class_url}}" class="button-df cc-danger cc-lg">JOIN LIVE
                    CLASS</a></div>
            <div data-w-id="c69665fd-1a63-6c2c-6f15-dfd6f10977fa" style="opacity:0"
                class="flex-width-2-3 flex-space-between">
                <div class="flex-width-1-2 flex-vr-center"><a href="{{route('c_s',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Course Syllabus</h4>
                    </a></div>
                <div class="flex-width-1-2 flex-vr-center"><a href="{{route('c_n',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Class News</h4>
                    </a></div>
                <div class="flex-width-1-2 flex-vr-center"><a href="{{route('c_r',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Course Recording</h4>
                    </a></div>
                <div class="flex-width-1-2 flex-vr-center"><a href="{{route('c_t_q',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Tests and Quizzes</h4>
                    </a></div>
                {{-- <div class="flex-width-1-2 flex-vr-center"><a href="{{route('m_t',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Meet the Teacher</h4>
                    </a></div> --}}

                <div class="flex-width-1-2 flex-vr-center">
                    <a href="{{route('c_p_i',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Payments and Invoice</h4>
                    </a>
                </div>

                <div class="flex-width-1-2 flex-vr-center">
                    <a href="{{route('chat_student',$course->id)}}"
                        class="card-sm cc-student-board w-inline-block">
                        <h4 class="heading-h5 text-nv text-center">Chat Now</h4>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
