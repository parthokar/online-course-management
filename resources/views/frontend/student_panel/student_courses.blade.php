@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <section id="after-student-login" class="section mt-100">
        <div class="w-layout-blockcontainer container w-container">
            <div class="flex-space-between">


                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">My courses</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>



                @foreach($data as $item)
                <a data-w-id="ca3cfd2b-c5f7-292e-4880-31ce73db352c" style="opacity:0"
                    href="{{route('p_s_c_d_route',$item->course->id)}}" class="flex-width-1-3 cc-tr-courses cc-with-bg w-inline-block">
                    <div class="card-sm"><img
                            src="{{asset('dashboard/course/'.$item->course->course_image)}}"
                            loading="lazy"
                            sizes="(max-width: 479px) 79vw, (max-width: 767px) 87vw, (max-width: 991px) 90vw, 26vw"
                            srcset="{{asset('dashboard/course/'.$item->course->course_image)}}"
                            alt="" class="image-wrapper" /></div>
                    <div class="button-df cc-enroll bg-nv">{{$item->course->course_title}}</div>
                </a>
                @endforeach
            </div>
        </div>
    </section>
@endsection
