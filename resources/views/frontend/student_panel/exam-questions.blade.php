@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <div id="test-quiz" class="section mt-100">
        <div class="container w-container">
            <div data-w-id="94572a31-a6cd-a047-0211-0981819ed6de" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">Test</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            

            <div class="flex-space-between">
                <div class="full_width">
                    <div class="w-form">
                        <form action="{{ route('student-exam.store') }}" id="email-form-2" name="email-form-2"
                            data-name="Email Form 2" method="post" data-wf-page-id="5d58db2bdc65b87dd3ff4c38"
                            data-wf-element-id="5836b62b-5e34-7e1b-3fff-a763de1db8b1">

                            @csrf
                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                            <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                            <input type="hidden" name="exam_name" value="{{ $exam->title }}">
                            <input type="hidden" name="exam_time" value="{{ $exam->exam_date . ' ' . $exam->exam_time }}">



                            @foreach ($data as $key => $questions)
                                <input type="hidden" name="question_id[]" value="{{ $questions->id }}">
                                <input type="hidden" name="question[]" value="{{ $questions->question }}">


                                <label for="email" class="paragraph cc-question-title">
                                    {{ ++$key }}. {{ $questions->question }}
                                </label>


                                @if (!empty(json_decode($questions->options)))
                                @foreach (json_decode($questions->options) as $option)
                                    <label class="paragraph cc-question-options w-radio">
                                        <input id="option_{{ $questions->id }}_{{ $loop->index }}" type="radio"
                                            name="answer_{{ $questions->id }}" value="{{ $option }}" required=""
                                            class="w-form-formradioinput w-radio-input" />
                                        <span class="w-form-label" for="radio">{{ $option }}</span>
                                    </label>
                                @endforeach
                                @else
                                <textarea cols="10" rows="10" class="form-control" name="answer_{{ $questions->id }}" required></textarea>
                                @endif
                            @endforeach
                            <input type="submit" value="Submit" data-wait="Please wait..."
                                class="button-df mt-20 w-button" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
