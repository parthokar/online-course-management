@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <section id="after-student-login" class="section mt-100">
        <div class="w-layout-blockcontainer container w-container">
            <div class="flex-space-between">
                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">{{$course->course_title}}</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>


                 <p> {!! $course->contents !!}
                   </p>




            </div>
        </div>
    </section>
@endsection
