@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <section id="after-student-login" class="section mt-100">
        <div class="w-layout-blockcontainer container w-container">
            <div class="flex-space-between">
                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">{{ $course->course_title }}</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>

              @if($data->count()>0)
                @foreach ($data as $item)
                    <div class="flex-width-1-3 cc-free-vdo">
                        <div class="w-embed-youtubevideo">
                            <video width="100%" height="300px" controls>
                                <source src="{{ asset('dashboard/record_video/' . $item->video_path) }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                        <div class="card-sm cc-border cc-warning">
                            <h4 class="heading-h5">{{ $item->video_title }} <br>
                                <a target="_blank" href="{{ asset('dashboard/record_video/' . $item->video_path) }}">Download
                                    Video</a>
                            </h4>
                        </div>
                    </div>
                @endforeach
                @else
                No Video Found 
               @endif



            </div>
        </div>
    </section>
@endsection
