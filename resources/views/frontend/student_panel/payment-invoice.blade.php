@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<style>
    /* Card Styles */
    .card {
        width: 400px;
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 20px;
        margin: 20px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        background-color: #f9f9f9;
    }

    .card h2 {
        margin-bottom: 10px;
        color: #333;
    }

    /* Table Styles */
    .payment-table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

    .payment-table th, .payment-table td {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: left;
    }

    .payment-table th {
        background-color: #f2f2f2;
    }
</style>
    <section id="after-student-login" class="section mt-100">
        <div class="w-layout-blockcontainer container w-container">
            <div class="flex-space-between">
                <div data-w-id="263425e3-e7a8-c335-d69d-66ae00d71b22" style="opacity:0"
                    class="full_width text-center flex-center">
                    <h2 class="heading-h2 cc-section-title text-nv">{{$course->course_title}}</h2>
                    <div class="divider-full cc-small cc-section-title bg-nv"></div>
                </div>
                <div class="card" style="width: 100%">
                    <h2>Course Details</h2>
                    <p><strong>Course Title:</strong> {{$course->course_title}}</p>
                    <p><strong>Course Price:</strong> {{$course->course_price}}</p>
                    <!-- Add more course details here if needed -->

                    <h2>Payment List</h2>
                    <table class="payment-table">
                        <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Transaction id</th>
                                <th>Payment date</th>
                                <th>Payment amount</th>
                                <th>Payment by</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $key=>$item)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$item->transaction_id}}</td>
                                <td>{{date('F-d-Y h:i a',strtotime($item->created_at))}}</td>
                                <td>{{$item->course->currency}} {{number_format($item->amount, 2, '.', ',')}} </td>
                                <td>{{optional($item->paymentBy)->name}}</td>
                            </tr>
                            @endforeach
                            <tfoot>
                                <tr>
                                    <th>Course Price:</th>
                                    <th id="total-payment">{{$course->currency}} {{number_format($course->course_price, 2, '.', ',')}}</th>
                                </tr>
                                <tr>
                                    <th>Total Payment:</th>
                                    <th id="total-payment">{{$course->currency}} {{number_format($total_amount, 2, '.', ',')}} </th>
                                </tr>
                                <tr>
                                    <th>Total Due:</th>
                                    <th id="total-due">{{$course->currency}} {{number_format($course->course_price-$total_amount, 2, '.', ',')}} </th>
                                </tr>
                            </tfoot>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
