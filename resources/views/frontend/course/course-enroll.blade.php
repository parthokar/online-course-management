@extends('layouts.app')
@section('title') Course enroll @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course enroll</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif

        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif

         <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Course</th>
                        <th>Discount code</th>
                        <th>Discount amount</th>
                        <th>Created</th>
                        <th width="100px">Exam</th>
                        <th width="100px">Quiz</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
    </section>
  </div>

  @section('script')

  <script>
    $(document).ready(function() {
               $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('student-course-enroll.index') }}",
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {data: 'courses', name: 'courses'},
                {data: 'discount_code', name: 'discount_code'},
                {data: 'discount_amount', name: 'discount_amount'},
                {data: 'created', name: 'created'},
                {data: 'action_exam', name: 'action_exam', orderable: false, searchable: false},
                {data: 'action_quiz', name: 'action_quiz', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
  </script>
  @endsection
@endsection


