@extends('layouts.app')
@section('title') Course enroll @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Course enroll</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">



        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Tag Line</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Duration</th>
                    <th>Instructor</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</th>
                    <td>{{$data->course->category->course_category}}</th>
                    <td>{{$data->course->course_title}}</th>
                    <td>{{$data->course->tag_line}}</th>
                    <td>{{$data->course->start_date}}</th>
                    <td>{{$data->course->end_date}}</th>
                    <td>{{$data->course->duration}}</th>
                    <td>{{$data->course->created_user->name}}</th>
                    <td>{{$data->course->course_price}}</th>
                    <td><img src="{{asset('dashboard/course/'.$data->course->course_image)}}" width="100px" height="100px" class="img-fluid"></th>
                    <td>{{$data->course->status==1?'Active':'In active'}}</th>
                </tr>
            </tbody>
        </table>


      </div>
    </section>
  </div>
@endsection


