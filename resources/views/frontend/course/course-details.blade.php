@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
    <!-- /* Course details page view section */ -->
    <div id="faq" class="section mt-100">

        <div class="divider-full"></div>
        <div id="about-us" class="section wf-section">
            <div class="container w-container">
                <div class="flex-space-between">
                    <div data-w-id="fa1a173d-e2b7-159c-57ac-8bd02152c429" style="opacity:0"
                        class="full_width text-center flex-center mb-20">
                        <h2 class="heading-h2 cc-section-title text-nv">Course details</h2>

                

                        <div class="divider-full cc-small cc-section-title bg-nv"></div>
                    </div>

                    <div class="flex-width-1-2">

                        <img src="{{ asset('dashboard/course/' . $data->course_image) }}" loading="lazy"
                            sizes="(max-width: 479px) 100vw, (max-width: 767px) 92vw, (max-width: 991px) 94vw, 45vw"
                            srcset="{{ asset('dashboard/course/' . $data->course_image) }}" alt="course image"
                            class="image-wrapper" />
                    </div>
                    <div class="flex-width-1-2 border-df cc-vr-left">
                        <div class="paragraph cc-large mb-20"><strong>Course Details:</strong> <br />Title:
                            {{ $data->course_title }}
                            <br />Date: {{ date('d-F-Y',strtotime($data->start_date)) }} To: {{ date('d-F-Y',strtotime($data->end_date)) }}
                            <br />Price:
                            {{ $data->currency }} @if ($data->free != 1)
                                {{ $data->course_price }}
                            @else
                                <del style="color: red">{{ $data->course_price }}</del>
                            @endif
                            <strong></strong>
                        </div>
                        <form method="post" action="{{ route('student-course-enroll.store') }}">
                            @csrf
                            <input type="hidden" name="course_id" value="{{ $data->id }}">
                            <button class="button-df mb-20">Enroll Now</button>
                        </form>

                        {{-- <a href="#" class="button-df bg-df">Purchase Now</a> --}}
                    </div>
                    <div class="full_width">
                        <div class="paragraph cc-large mb-20"><strong>Course Content: </strong></div>
                        <ul role="list" class="list">
                            {!! $data->contents !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
