@extends('layouts.app_front')
@section('title')  @endsection
@section('content')
<div id="trending-courses" class="section mt-100">
    <div class="container w-container">
        <div class="flex-space-between">
            <div data-w-id="71d39c3a-220e-0d04-5d3e-d5d9732b0c89" style="opacity:0"
                class="full_width text-center flex-center">
                <h2 class="heading-h2 cc-section-title">All Courses</h2>
                <div class="divider-full cc-small cc-section-title mb-20"></div>
            </div>
            @foreach($data as $item)
            <div data-w-id="3915ab21-a290-31a8-1697-08c001fefcd2" style="opacity:0"
                class="flex-width-1-3 cc-tr-courses">
                <div class="card-sm"><img
                        src="{{asset('dashboard/course/'.$item->course_image)}}"
                        loading="lazy"
                        sizes="(max-width: 479px) 79vw, (max-width: 767px) 87vw, (max-width: 991px) 90vw, 26vw"
                        srcset="{{asset('dashboard/course/'.$item->course_image)}}"
                        alt="" class="image-wrapper border-radius mb-20" /><a href="{{route('course_details',$item->id)}}"
                        class="button-df cc-enroll" data-w-id="1b86f29e-c341-a3c3-3453-969d88d36c0e">Enroll Now</a></div><a href="{{route('course_details',$item->id)}}"
                    class="button-df cc-enroll bg-nv">{{$item->course_title}}</a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
