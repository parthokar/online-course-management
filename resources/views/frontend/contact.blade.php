@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<div id="contact" class="section mt-100">
    <div class="container w-container">
        <div class="flex-space-between">
            <div data-w-id="d52b94e2-fcf1-018b-e424-d5c05334fb24" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">Contact Us</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="flex-width-2-3">
                <div class="w-form">
                    <form id="email-form" action="{{ route('contact_store_route') }}" method="post">
                        @csrf

                        <label for="name">Name</label><input type="text" class="w-input" maxlength="256"
                            name="name" data-name="Name" placeholder="" id="name"
                            required="" />

                            <label for="email">Email</label><input type="email"
                            class="w-input" maxlength="256" name="email" data-name="Email" placeholder=""
                            id="email" required="" />

                            <label for="phone">Phone</label><input type="email"
                            class="w-input" name="phone" data-name="phone" placeholder=""
                            id="phone" required="" />

                            <label for="subject">Subject</label><input
                            type="text" class="w-input" maxlength="256" name="subject" data-name="subject"
                            placeholder="" id="subject" required="" />

                            <label for="details">Details</label>
                        <textarea required="" placeholder="What you want to know?" maxlength="5000" id="details" name="message"
                            data-name="details" class="w-input"></textarea><input type="submit" value="Submit"
                            data-wait="Please wait..." class="button-df mt-20 w-button" />
                    </form>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
            </div>
            <div class="flex-width-1-3">
                <div class="paragraph cc-large mb-20"><strong>Contact Details:</strong> <br />Location: 7062 45 Ave.
                    Woodside, NY 11377<br />Phone: +1 646-321-7507<br />Email: info@expertautomationteam.com<br />
                </div>
            </div>
            <div class="full_width">
                <div class="w-widget w-widget-map" data-widget-style="roadmap" data-widget-latlng="" aria-label=""
                    data-enable-scroll="true" role="region" title="" data-enable-touch="true" data-widget-zoom="12"
                    data-widget-tooltip=""></div>
            </div>
        </div>
    </div>
</div>
@endsection
