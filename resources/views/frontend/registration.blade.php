@extends('layouts.app_front')
@section('title')  @endsection
@section('content')

<div id="about-us" class="section mt-100">
    <div id="registration" class="section">
        <div class="container w-container">
            <div class="flex-space-between">
                <div data-w-id="3a25ada7-037e-f3e3-739a-77b04b494cc7" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;" class="full_width text-center flex-center mb-20">
                    <h2 class="heading-h2 cc-section-title text-nv">
                        User Registration</h2>
                        <div class="divider-full cc-small cc-section-title bg-nv">
                            </div>
                        </div>
                        <div class="full_width">
                            <div class="w-form">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="flex-space-between cc-small">
                                        <div class="flex-width-12">
                                            <label for="student_name">Name</label>
                                            <input type="text" class="w-input" maxlength="256" name="name" value="{{ old('name') }}" data-name="student_name" placeholder="Enter your Name" id="student_name" required="">
                                        </div>
                                        <div class="flex-width-12">
                                            <label for="student_email">Email</label>
                                            <input type="email" class="w-input" maxlength="256" name="email" value="{{ old('email') }}" data-name="student_email" placeholder="Enter your valid email here" id="student_email" required="">
                                            @error('email')

                                                <strong>{{ $message }}</strong>

                                        @enderror
                                        </div>
                                        <div class="flex-width-12">
                                            <label for="student_phone">Phone</label>
                                            <input type="tel" class="w-input" maxlength="256" name="phone" value="{{ old('phone') }}" data-name="student_phone" placeholder="Enter your phone number" id="student_phone" required="">
                                            @error('phone')

                                                <strong>{{ $message }}</strong>

                                        @enderror
                                        </div>
                                        <div class="flex-width-12"><label for="student_current_job">Password</label>
                                            <input type="text" class="w-input" maxlength="256" name="password" data-name="student_current_job" placeholder="Enter password" id="student_current_job" required="">
                                        </div>
                                    </div>
                                    <input type="submit" value="Register" data-wait="Please wait..." class="button-df mt-20 w-button">
                                </form>
                                <div class="w-form-done" tabindex="-1" role="region" aria-label="Email Form success">
                                    <div>
                                        Thank you! Your submission has been received!
                                    </div>
                                </div>
                                <div class="w-form-fail" tabindex="-1" role="region" aria-label="Email Form failure">
                                    <div>Oops! Something went wrong while submitting the form.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>
@endsection
