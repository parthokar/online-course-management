@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<div id="contact" class="section mt-100">
    <div class="container w-container">
        <div class="flex-space-between">
            <div data-w-id="d52b94e2-fcf1-018b-e424-d5c05334fb24" style="opacity:0"
                class="full_width text-center flex-center mb-20">
                <h2 class="heading-h2 cc-section-title text-nv">Announcement</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>
            <div class="flex-width-2-3">
                <div class="w-form">



                    @foreach ($data as $item)
                    @if (date('Y-m-d')>$item->end_date)
                      @else
                        <div class="flex-width-1-2 border-df">
                            <div class="paragraph cc-large text-center">
                                <strong>{{ $item->topics }} </strong><br />
                                <img src="{{ asset('dashboard/announce/' . $item->image) }}" width="50%" height="50%"
                                    class="img-fluid"><br />
                                    <p>Url: <a target="_blank" href="{{$item->url}}">{{$item->url}}</a> </p>
                                <p>{{ $item->start_date }} to {{ $item->end_date }}</p><br>
                                {{ $item->details }}<br /><strong>
                            </div>
                        </div>
                    @endif
                @endforeach













                </div>
            </div>
        </div>
    </div>
</div>
@endsection
