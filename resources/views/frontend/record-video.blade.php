@extends('layouts.app_front')
@section('title')
@endsection
@section('content')
<section id="after-student-login-record" class="section mt-100">
    <div class="w-layout-blockcontainer container w-container">
        <div class="flex-space-between">
            <div data-w-id="366ac2e7-447f-feee-0cff-635e83f8b888" style="opacity:0"
                class="full_width text-center flex-center">
                <h2 class="heading-h2 cc-section-title text-nv">Record Video</h2>
                <div class="divider-full cc-small cc-section-title bg-nv"></div>
            </div>

            @foreach($data as $item)
            <div class="flex-width-1-3 cc-free-vdo">
                <div  class="w-embed-youtubevideo">

                    <video width="100%" height="300px" controls>
                        <source src="{{ asset('dashboard/record_video/'.$item->video_path) }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>


                    </div>
                <div class="card-sm cc-border cc-warning">
                    <h4 class="heading-h5">Course: {{$item->course_title}}  Publish Date: {{$item->publish_date}} <a target="_blank" href="{{$item->video_url}}">Video Link</a>  </h4>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>
@endsection
