<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment Reminder</title>
    <style>
        /* Add your CSS styles here for better email formatting */
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #ffffff;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }

        .header {
            text-align: center;
        }

        .header img {
            max-width: 150px;
            margin: 20px auto;
            display: block;
        }

        .content {
            margin-top: 20px;
        }

        p {
            font-size: 16px;
            line-height: 1.5;
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="content">
            <p>Hi {{ $student->name }},</p>
            <p>You have {{$student->currency}} {{$student->due_amount}} due. Please make a payment in the next 2 days to avoid any late fees.</p>
            <p>Thank you.</p>
            <p>Expert Automation Team</p>
        </div>
        <div class="header">
            <img src="https://uploads-ssl.webflow.com/5d58db2bdc65b81d7aff4c37/64501143a732bb4cdf6f1e92_Logo%2010%20Transparent.png" alt="Company Logo">
        </div>
    </div>
</body>
</html>
