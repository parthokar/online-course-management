<!DOCTYPE html>
<html>
<head>
    <title>User Email</title>
</head>
<body>
    <p>Hello {{ $student->name }},</p>
    <p>{!! $body !!}
    </p>
</body>
</html>
