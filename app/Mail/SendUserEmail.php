<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendUserEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $student;
    public $subject;
    public $body;

    public function __construct($student,$subject,$body)
    {
        $this->student = $student;
        $this->subject = $subject;
        $this->body = $body;
    }

    public function build()
    {
        return $this->view('emails.user-send-email')
                    ->subject($this->subject);
    }
}
