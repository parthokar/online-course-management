<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentAddPaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $amount;
    public $course;
    public $due_amount;
    public $currency;


    public function __construct($name,$email,$amount,$course,$due_amount,$currency)
    {
        $this->name = $name;
        $this->email = $email;
        $this->amount = $amount;
        $this->course = $course;
        $this->due_amount = $due_amount;
        $this->currency = $currency;
    }

    public function build()
    {

        return $this->markdown('emails.payment_add')
        ->subject('Payment Reminder');
    }
}
