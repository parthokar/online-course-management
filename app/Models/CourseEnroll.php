<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Course;
use App\Models\PromoCode;

class CourseEnroll extends Model
{
    use HasFactory;

    public function student(){
      return $this->belongsTo(User::class);
    }

    public function course(){
      return $this->belongsTo(Course::class);
    }

    public function promo(){
        return $this->belongsTo(PromoCode::class,'promo_id');
    }
}
