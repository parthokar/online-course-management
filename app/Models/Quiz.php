<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\QuestionQuiz;
use App\Models\Course;

class Quiz extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id',
        'title',
        'quiz_date',
        'quiz_time',
        'details',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function questionData(){
        return $this->hasMany(QuestionQuiz::class,'quiz_id');
    }

    public function courseName(){
        return $this->belongsTo(Course::class,'course_id');
    }
}
