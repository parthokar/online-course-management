<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Course;

class Slack extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id',
        'channel_name',
        'channel_type',
        'status',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

}
