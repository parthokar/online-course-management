<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\CourseCategory;
use App\Models\User;

class Course extends Model
{

    protected $fillable = [
        'category_id',
        'teacher_id',
        'promo_id',
        'course_title',
        'tag_line',
        'start_date',
        'end_date',
        'duration',
        'duration',
        'instructor',
        'details',
        'contents',
        'requirements',
        'course_image',
        'course_price',
        'currency',
        'discount_id',
        'free',
        'created_by',
        'status',
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    public function category(){
        return $this->belongsTo(CourseCategory::class);
    }

    public function created_user(){
        return $this->belongsTo(User::class,'teacher_id');
    }
    use HasFactory;
}
