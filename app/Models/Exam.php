<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use App\Models\Course;


class Exam extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id',
        'title',
        'exam_date',
        'exam_time',
        'details'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function questionData(){
      return $this->hasMany(Question::class,'exam_id');
    }

    public function courseName(){
        return $this->belongsTo(Course::class,'course_id');
    }
}
