<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class LogActivity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'agent', 'user_id','is_login'
    ];

    public function student(){
        return $this->belongsTo(User::class,'user_id');
    }
}
