<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PromoCode extends Model
{
    protected $fillable = [
        'code',
        'currency',
        'amount',
        'date_time',
        'created_by',
        'status',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }
    use HasFactory;

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }
}
