<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MeetTeacher;
use App\Models\User;

class MeetTeacherStudentRequest extends Model
{
    use HasFactory;

    public function teacher(){
      return $this->belongsTo(User::class);
    }

    public function meetTime(){
        return $this->belongsTo(MeetTeacher::class,'time_id');
    }
}
