<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Announcement extends Model
{
    use HasFactory;
    protected $fillable = [
        'topics',
        'start_date',
        'end_date',
        'image',
        'url',
        'details',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }
}
