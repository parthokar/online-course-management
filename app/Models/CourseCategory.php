<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CourseCategory extends Model
{

    protected $fillable = [
        'course_category',
        'created_by',
        'status',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }
    use HasFactory;

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }
}
