<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Course;

class StudentPayment extends Model
{

    protected $fillable = [
        'student_id',
        'course_id',
        'transaction_id',
        'amount',
    ];
    use HasFactory;

    public function student(){
      return $this->belongsTo(User::class);
    }

    public function course(){
      return $this->belongsTo(Course::class);
    }

    public function paymentBy(){
        return $this->belongsTo(User::class,'payment_by');
    }
}
