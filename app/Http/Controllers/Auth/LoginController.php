<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\LogActivity;
use Carbon\Carbon;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        //dd($request->all());
        $input = $request->all();

        $this->validate($request, [
            'login_email' => 'required|email',
            'login_password' => 'required',
        ]);

        $role_user = User::where('email', $input['login_email'])
            ->select('slug', 'users.id as user_id')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->first();
        if (!isset($role_user)) {
            return redirect()->route('home_route')->with('error','Email-Address And Password Are Wrong.');
        }
        if ($role_user->slug == 'student-role') {
            $check_login = LogActivity::
                where('user_id', $role_user->user_id)
                ->orderBy('id', 'DESC')
                ->first();
            if (isset($check_login)) {
                if ($check_login->is_login == 1) {
                    return redirect()->route('home_route')->with('error','Email-Address And Password Are Wrong.');
                }
            }
        }

        if (auth()->attempt(array('email' => $input['login_email'], 'password' => $input['login_password'], 'status' => 1))) {
            $role = User::where('email', $input['login_email'])
                ->select('slug')
                ->join('roles', 'roles.id', '=', 'users.role_id')
                ->first();
            $is_login = 0;
            if ($role->slug == 'student-role') {
                $is_login = 1;
            }
            $log = [];
            $log['subject'] = 'Login Information';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
            $log['is_login'] = $is_login;
            $log['created_at'] = Carbon::now('Asia/Dhaka');
            LogActivity::create($log);
            if ($role->slug == 'student-role') {
                return redirect()->route('p_s_c_route');
            }
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->route('home_route')->with('error','Email-Address And Password Are Wrong.');
        }

    }

    public function logout(Request $request)
    {

        $role = User::where('role_id', auth()->user()->role_id)
            ->select('slug')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->first();
        if ($role->slug == 'student-role') {
            $log = [];
            $log['subject'] = 'Logout Information';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
            if ($role->slug == 'student-role') {
                $log['is_login'] = 0;
            }
            $log['created_at'] = Carbon::now('Asia/Dhaka');
            LogActivity::create($log);
        }
        Auth::logout();
        return redirect('/');
    }
}
