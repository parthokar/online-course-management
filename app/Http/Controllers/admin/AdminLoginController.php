<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{


    public function login(){
        return view('auth.login');
    }


     //login
     public function loginAction(Request $request){
        $input = $request->all();
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
                if(auth()->user()->role_id==1){
                    return redirect()->route('admin.dashboard');
                }

                elseif(auth()->user()->role_id==3){
                    return redirect()->route('admin.dashboard');
                }
                else{
                    return redirect()->route('admin_login')
                    ->with('error','Email-Address And Password Are Wrong.');
                }

        }else{
                return redirect()->route('admin_login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
   }
}
