<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use DataTables;

class CompanyController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Company::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('logo', function($row){
                        return '<img src="'.asset('dashboard/company/'.$row->logo).'" width="50%" height="50%" class="img-fluid">';
                   })
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('company.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['logo','status','action'])
                    ->make(true);
        }
        return view('dashboard.company.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.company.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
        ]);

       // Get the file from the request
       $image = $request->file('logo');

       // Generate a unique name for the file to avoid conflicts
       $filename = time().'.'.$image->getClientOriginalExtension();

       // Save the image to the public folder
       $image->move('dashboard/company/', $filename);

       Company::create([
           'logo'=>$filename,
           'status'=>$request->status,
       ]);

        return redirect()->route('company.index')->with('success','Company has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $company = Company::find($id);
        return view('dashboard.company.edit',compact('company'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $company = Company::find($id);

        $request->validate([
            'course_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
        ]);

        if ($request->hasFile('logo')) {
                  // Get the file from the request
            $image = $request->file('logo');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/company/', $filename);
        }else{
            $filename = $company->logo;
        }

        Company::where('id',$id)->update([
            'logo'=>$filename,
            'status'=>$request->status,
        ]);

        return redirect()->route('company.index')->with('success','Company Has Been updated successfully');
    }

}
