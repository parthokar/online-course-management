<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Notice;

use App\Models\Course;

use App\Models\Role;

use DataTables;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data = Notice::
                join('users','users.id','=', 'notices.created_by')
                ->join('courses', 'courses.id', '=', 'notices.course_id')
                ->select('notices.*','users.name','courses.course_title')
                ->orderBy('notices.id', 'desc')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('course', function ($row) {
                    return $row->course_title;
                })

                ->addColumn('teacher', function ($row) {
                    return $row->name;
                })

                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return 'Active';
                    } else {
                        return 'In active';
                    }
                })

                ->addColumn('action', function ($row) {
                    return '<a href="' . route('notice.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })

                ->rawColumns(['course','teacher','status','action'])

                ->make(true);
        }
        return view('dashboard.notice.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $role = Role::where('id',auth()->user()->role_id)->select('slug')->first();
        if($role->slug=='admin-role'){
            $course = Course::
            select('id', 'course_title')
            ->get();
        }else{
            $course = Course::where('teacher_id',auth()->user()->id)
            ->select('id', 'course_title')
            ->get();
        }

        return view('dashboard.notice.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'course_id' => 'required',
            'created_by' => 'required',
            'topic' => 'required',
            'description' => 'required',
            'status' => 'required',
            'date_time' => 'required',
        ]);

        Notice::create($request->post());

        return redirect()->route('notice.index')->with('success', 'Notice has been created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::where('teacher_id', auth()->user()->id)->select('id', 'course_title')->get();
        $data = Notice::find($id);
        return view('dashboard.notice.edit', compact('course', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Notice::find($id);

        $request->validate([
            'course_id' => 'required',
            'created_by' => 'required',
            'topic' => 'required',
            'description' => 'required',
            'status' => 'required',
            'date_time' => 'required',
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('notice.index')->with('success', 'Notice Has Been updated successfully');
    }

}
