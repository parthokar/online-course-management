<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slack;
use App\Models\Course;
use App\Models\CourseEnroll;
use App\Models\SlackMessage;

class SlackController extends Controller
{


    public function joinCall(){
        return view('dashboard.video_call.index');
    }


    //channel list
    public function channelList(){


            $channel = Slack::with('course')
            ->where('slacks.status',1)
            ->leftjoin('courses','courses.id','=','slacks.course_id')
            ->select('slacks.id as main_id','courses.*','channel_name','channel_type')
            ->get();


       return view('dashboard.slack.channel-list',compact('channel'));
    }

    //slack message
    public function slackMessage($id){

        $channel_info = Slack::where('id',$id)->first();

        if($channel_info->channel_type==2){
            return view('dashboard.slack.message',compact('id','channel_info'));
        }
        if($channel_info->channel_type==1){
           $course_enroll = CourseEnroll::
           where('course_id',$channel_info->course_id)
           ->where('student_id',auth()->user()->id)
           ->first();
           if(!isset($course_enroll)){
             return redirect()->back()->with('error','Channel is restricted');
           }else{
            return view('dashboard.slack.message',compact('id','channel_info'));
           }
        }
        return view('dashboard.slack.message',compact('id','channel_info'));
    }

    public function slackMessageAjaxStore(Request $request){
       $data = new SlackMessage;
       $data->slack_id = $request->slack_id;
       $data->user_id = auth()->user()->id;
       $data->message = $request->message;
       $data->save();
    }

    //slack message ajax
    public function slackMessageAjax($id){
        $message = SlackMessage::with('user')
        ->where('slack_id',$id)
        ->orderBy('id','DESC')
        ->get();
        return response()->json($message);
    }

    public function channelCreate(){
      $course = Course::all();
      return view('dashboard.admin.channel.create',compact('course'));
    }

    public function channelStore(Request $request){

        if($request->channel_type==1){
           if(!isset($request->course_id)){
            return redirect()->back()->with('error','If you want to create restricted channel at first please select course');
           }
        }

        Slack::create($request->post());

        return redirect()->route('channel_route')->with('success','Channel has been created successfully.');
    }
}
