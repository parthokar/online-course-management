<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Exam;
use App\Models\Course;
use App\Models\StudentExam;
use Carbon\Carbon;
use DB;


class StudentExamController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $exam_details = Exam::find($request->exam_id);
        $dateTime = $exam_details->exam_date . ' ' . $exam_details->exam_time;

        //check exam date is current date to perform exam
        if (date('Y-m-d') != $exam_details->exam_date) {
            return redirect()->route('c_t', [
                'course_id' => $request->course_id,
                'test_id' => $request->exam_id,
            ])->with('error', 'You can not attend exam today because the exam date is ' . $exam_details->exam_date);
        }

        //check exam already given
        $count = StudentExam::where('user_id', auth()->user()->id)
            ->where('course_id', $request->course_id)
            ->where('exam_id', $request->exam_id)
            ->whereDate('exam_time', date('Y-m-d'))
            ->count();


        if ($count > 0) {
            return redirect()->route('c_t', [
                'course_id' => $request->course_id,
                'test_id' => $request->exam_id,
            ])->with('error', 'You already submit answer.');
        }

        foreach ($request->question_id as $key => $item) {
            $correct_answer = DB::table('exam_questions')
                ->where('id', $request->question_id[$key])
                ->first();

            // Construct the answer field name based on the question index
            $answer_key = 'answer_' . $request->question_id[$key];

            // Use the constructed answer field name to access the answer from the request
            $answer = $request->$answer_key;

            $answer_data = new StudentExam;
            $answer_data->user_id = auth()->user()->id;
            $answer_data->course_id = $request->course_id;
            $answer_data->exam_id = $request->exam_id;
            $answer_data->question = $request->question[$key];
            $answer_data->question_id = $request->question_id[$key];
            $answer_data->answer = $answer; // Use the extracted answer
            $answer_data->exam_time = $request->exam_time;
            $answer_data->exam_name = $request->exam_name;
            $answer_data->correct_answer = $correct_answer->answer;
            $answer_data->status = 1;
            $answer_data->save();
        }

        //Alert::success('Success', 'Operation was successful.');

        return redirect()->route('get_exam_result', $request->course_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $exam = Exam::where('id', $id)->first();
        $course = Course::where('id', $exam->course_id)->first();
        $data = Question::where('exam_id', $id)->where('course_id', $exam->course_id)->get();
        $answer = StudentExam::where('user_id', auth()->user()->id)
            ->where('exam_id', $id)
            ->where('course_id', $exam->course_id)
            ->where('exam_time', $exam->exam_date . ' ' . $exam->exam_time)
            ->where('status', 1)
            ->count();

        if ($answer > 0) {
            return redirect()->route('course-enroll.index')->with('error', 'You already submit answer');
        }

        $dateTime = $exam->exam_date . ' ' . $exam->exam_time;

        if (Carbon::now()->toDateTimeString() >= $dateTime) {
            return redirect()->route('course-enroll.index')->with('error', 'Exam Time expire');
        }

        return view('dashboard.student.exam.create_exam', compact('exam', 'course', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
