<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Course;
use DataTables;
use DB;

class ExamController extends Controller
{

   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Exam::leftjoin('courses','courses.id','=','exams.course_id')->select('course_title','exams.*')->orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    // ->addColumn('course_name', function($row){
                    //     return optional($row->courseName->course_title);
                    //  })
                     ->addColumn('action', function($row){
                        return '<a href="' . route('exam.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action',])
                    ->make(true);
        }
        return view('dashboard.teacher.exam.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $course = Course::where('status',1)->orderBy('id','DESC')->get();
        $exam = Exam::orderBy('id','desc')->get();
        return view('dashboard.teacher.exam.create',compact('course','exam'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'exam_date' => 'required',
            'exam_time' => 'required',
            'details' => 'required',
        ]);

        if($request->input('questions')==''){
            $request->validate([
                'question' => 'required',
            ]);
        }

        $exam = Exam::create($request->post());


        $course = Exam::find($exam->id);
        $questions = $request->input('questions');
        $options = $request->input('options');
        $answers = $request->input('answers');
        $answers_new = $request->input('answer_new');


        foreach ($questions as $index => $question) {
            $examQuestion = new Question();
            $examQuestion->course_id = $course->course_id; // Replace with the actual exam ID
            $examQuestion->exam_id = $exam->id; // Replace with the actual exam ID
            $examQuestion->question = $question;

            // Check if options for this question are available
            if (isset($options[$index])) {
                $examQuestion->options = json_encode($options[$index]);
            }

            // Check if answer for this question is available
            if (isset($answers[$index])) {
                // Assuming there's only one answer for each question
                $examQuestion->answer = $answers[$index][0]; // Get the first element of the array
            }

            if (isset($answers_new[$index])) {
                // Assuming there's only one answer for each question
                $examQuestion->answer = $answers_new[$index][0]; // Get the first element of the array
            }

            if(isset($request->option_type)){
                $examQuestion->option_type = $request->option_type[$index];
            }

           $examQuestion->save();
        }

        return redirect()->route('exam.create')->with('success','Exam has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Exam  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $course = Course::where('status',1)->orderBy('id','DESC')->get();
        $category=Exam::with('questionData')->find($id);
        $question = Question::where('exam_id',$id)->where('course_id',$category->course_id)->get();
        $exam = Exam::orderBy('id','desc')->get();
        return view('dashboard.teacher.exam.edit',compact('question','course','category','exam'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=Exam::find($id);
        $request->validate([
            'course_id' => 'required',
            'title' => 'required',
            'exam_date' => 'required',
            'exam_time' => 'required',
            'details' => 'required',
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('exam.edit',$id)->with('success','Exam has been updated successfully.');
    }

       /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::table('exam_questions')->where('id',$id)->delete();
       return back();
    }

    public function examQDelete($id){
        DB::table('exam_questions')->where('id',$id)->delete();
        return back();
    }
}
