<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Role;

use App\Models\CourseEnroll;

use App\Models\Course;

use App\Models\User;

use App\Models\PromoCode;

use DataTables;

class CourseEnrollController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $role_slug = Role::find(auth()->user()->role_id);

            if($role_slug->slug=='student-role'){
                $data = CourseEnroll::with('student','course','promo')
                ->where('student_id',auth()->user()->id)->orderBy('id','desc')
                ->get();
            }else{
                $data = CourseEnroll::with('student','course','promo')
                ->orderBy('id','desc')
                ->get();
            }

            return Datatables::of($data)
                    ->addIndexColumn()

                    ->addColumn('created', function($row){

                         return date('d-m-Y h:i:s a',strtotime($row->created_at)) ;

                     })

                     ->addColumn('students', function($row){
                          return optional($row->student)->name;
                      })

                      ->addColumn('course_price', function($row){
                        return optional($row->course)->currency.' '. number_format(optional($row->course)->course_price, 2, '.', ',');
                      })


                      ->addColumn('courses', function($row){
                        return optional($row->course)->course_title;
                      })

                     ->addColumn('discount_code', function($row){
                        return optional($row->promo)->code;
                      })

                      ->addColumn('discount_amount', function($row){
                        return optional($row->promo)->currency.' '. number_format(optional($row->promo)->amount, 2, '.', ',');
                      })

                      ->addColumn('status', function($row){
                       return $row->status==1?'Active':'Inactive';
                    })


                    ->addColumn('action', function($row) use ($role_slug) {
                        $buttons = '<a href="' . route('course-enroll.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';

                        if ($role_slug->slug == 'student-role') {
                            $buttons .= '<a href="' . route('student-exam.show', $row->id) .'" class="extra-button btn btn-warning btn-sm">Online Exam</a> <a href="' . route('student-quiz.show', $row->id) .'" class="extra-button btn btn-primary btn-sm">Online Quiz</a>';
                        }

                        return $buttons;
                    })


                    ->rawColumns(['created','students','courses','course_price','discount_code','discount_amount','status','action'])
                    ->make(true);
        }
        return view('dashboard.course_enroll.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $course = Course::find($request->course_id);

        $check = CourseEnroll::where('student_id',auth()->user()->id)
        ->where('course_id',$request->course_id)
        ->count();

        if($check>0){
           return redirect()->route('course_details',$request->course_id)
           ->with('error','Course already enroll');
        }

        $courseEnroll = new CourseEnroll;

        $courseEnroll->student_id = auth()->user()->id;

        $courseEnroll->course_id  =  $request->course_id;

        $courseEnroll->save();

        return redirect()->route('course_details',$request->course_id)
        ->with('success','Course enroll successfull.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Announcement  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $enroll  = CourseEnroll::find($id);
        $student = Role::where('slug','student-role')
        ->join('users','users.role_id','=','roles.id')
        ->select('users.id','users.name')
        ->get();
        $course  = Course::all();
        $promo  = PromoCode::all();
        return view('dashboard.course_enroll.edit',compact('enroll','student','course','promo'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {

        CourseEnroll::where('id',$id)->update([
            'student_id'=>$request->student_id,
            'course_id'=>$request->course_id,
            'promo_id'=>$request->promo_id,
            'status'=>$request->status,
        ]);

        return redirect()->route('course-enroll.index')->with('success','Enroll information has been updated successfully');
    }
}
