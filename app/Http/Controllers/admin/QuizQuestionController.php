<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionQuiz;
use App\Models\Quiz;

class QuizQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $course = Quiz::find($request->quiz_id);
        $questions = $request->input('questions');
        $options = $request->input('options');
        $answers = $request->input('answers');
        $answers_new = $request->input('answer_new');

        foreach ($questions as $index => $question) {
            $examQuestion = new QuestionQuiz();
            $examQuestion->course_id = $course->course_id; // Replace with the actual exam ID
            $examQuestion->quiz_id = $request->quiz_id; // Replace with the actual exam ID
            $examQuestion->question = $question;

            // Check if options for this question are available
            if (isset($options[$index])) {
                $examQuestion->options = json_encode($options[$index]);
            }

            // Check if answer for this question is available
            if (isset($answers[$index])) {
                // Assuming there's only one answer for each question
                $examQuestion->answer = $answers[$index][0]; // Get the first element of the array
            }


            if (isset($answers_new[$index])) {
                // Assuming there's only one answer for each question
                $examQuestion->answer = $answers_new[$index][0]; // Get the first element of the array
            }

            if(isset($request->option_type)){
                $examQuestion->option_type = $request->option_type[$index];
            }

            $examQuestion->save();
        }
        return redirect()->route('quiz.create')->with('question-success', 'Quiz question has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (isset($request->questions)) {
            $course = Quiz::find($request->quiz_id);
            $questions = $request->input('questions');
            $options = $request->input('options');
            $answers = $request->input('answers');
            $answers_new = $request->input('answer_new');

            foreach ($questions as $index => $question) {
                $examQuestion = new QuestionQuiz();
                $examQuestion->course_id = $course->course_id; // Replace with the actual exam ID
                $examQuestion->quiz_id = $request->quiz_id; // Replace with the actual exam ID
                $examQuestion->question = $question;

                // Check if options for this question are available
                if (isset($options[$index])) {
                    $examQuestion->options = json_encode($options[$index]);
                }

                // Check if answer for this question is available
                if (isset($answers[$index])) {
                    // Assuming there's only one answer for each question
                    $examQuestion->answer = $answers[$index][0]; // Get the first element of the array
                }

                if (isset($answers_new[$index])) {
                    // Assuming there's only one answer for each question
                    $examQuestion->answer = $answers_new[$index][0]; // Get the first element of the array
                }

                if(isset($request->option_type)){
                    $examQuestion->option_type = $request->option_type[$index];
                }

                $examQuestion->save();
            }

        }

        return redirect()->route('quiz.edit', $id)->with('question-success', 'Quiz question has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
