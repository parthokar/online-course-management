<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Course;

use App\Models\Exam;

use App\Models\User;

use App\Models\CourseEnroll;

use App\Models\StudentExam;

use App\Models\StudentQuiz;

class StudentCertificateController extends Controller
{

    //get certificate
    public function index(){
        $course = Course::latest()->get();
        return view('dashboard.certificate.index',compact('course'));
    }

     //get quiz
     public function indexQuiz(){
        $course = Course::latest()->get();
        return view('dashboard.certificate.indexQuiz',compact('course'));
    }


    //get student course wise
    public function courseStudent($courseId){

       $data = CourseEnroll::where('course_id',$courseId)
       ->join('users','users.id','=','course_enrolls.student_id')
       ->select('users.id','users.name')
       ->groupBy('student_id')
       ->get();

       return response()->json($data);

    }

    //certificate generate
    public function generateCertificate(Request $request){

       $course = Course::where('id',$request->course_id)->first();

       $exam   = StudentExam::where('student_exams.course_id',$request->course_id)
       ->select('exam_name as title','exam_time')
                ->first();
      if(!isset($exam)){
        return redirect()->back()->with('error','No Exam Record Found');
      }

       $student = User::find($request->student_id);

       $data = StudentExam::
       whereYear('exam_time',date('Y',strtotime($request->year_month)))
       ->whereMonth('exam_time',date('m',strtotime($request->year_month)))
       ->where('course_id',$request->course_id)
       ->where('user_id',$request->student_id)
       ->select('student_exams.question','correct_answer as question_answer','student_exams.answer as student_answer')
       ->groupBy('student_exams.id')
       ->selectRaw('SUM(CASE WHEN student_exams.answer = student_exams.correct_answer THEN 1 ELSE 0 END) as total_correct_answers')
       ->selectRaw('SUM(CASE WHEN student_exams.answer != student_exams.correct_answer THEN 1 END) as total_wrong_answers')
       ->get();

       $score = $data->sum('total_correct_answers');
       $wrong = $data->sum('total_wrong_answers');


       if($data->count()==0){
          return redirect()->back()->with('error','No Exam Record Found');
       }

       return view('dashboard.certificate.certificate-download',compact('course','exam','data','student','score','wrong'));

    }


     //certificate generate
     public function generateCertificateQuiz(Request $request){

        $course = Course::where('id',$request->course_id)->first();

        $exam   = StudentQuiz::where('student_quizzes.course_id',$request->course_id)
                  ->select('quiz_name as title','quiz_time')
                 ->first();
       if(!isset($exam)){
         return redirect()->back()->with('error','No Quiz Record Found');
       }

        $student = User::find($request->student_id);

        $data = StudentQuiz::
        whereYear('quiz_time',date('Y',strtotime($request->year_month)))
        ->whereMonth('quiz_time',date('m',strtotime($request->year_month)))
        ->where('course_id',$request->course_id)
        ->where('user_id',$request->student_id)
        ->select('student_quizzes.question','student_quizzes.answer as student_answer','correct_answer as question_answer')
        ->groupBy('student_quizzes.id')
        ->selectRaw('SUM(CASE WHEN correct_answer = student_quizzes.answer THEN 1 ELSE 0 END) as total_correct_answers')
        ->selectRaw('SUM(CASE WHEN correct_answer != student_quizzes.answer THEN 1 END) as total_wrong_answers')
       ->get();

       $score = $data->sum('total_correct_answers');
       $wrong = $data->sum('total_wrong_answers');

        if($data->count()==0){
           return redirect()->back()->with('error','No Quiz Record Found');
        }

        return view('dashboard.certificate.certificate-download-quiz',compact('course','exam','data','student','score','wrong'));

     }



}
