<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use DataTables;

class PermissionController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = Permission::orderBy('name','ASC')->get();
            return Datatables::of($data)
                    ->addIndexColumn()

                    ->addColumn('action', function($row){
                        return '<a href="' . route('permission_edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.permission.index');
    }

    public function edit($id){
        $permission = Permission::find($id);
        return view('dashboard.permission.edit',compact('permission'));
    }

    public function update(Request $request,$id){
   
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        Permission::where('id',$id)->update([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);

        return redirect()->route('permission_list')->with('success','Permission has Been updated successfully');
    }
}
