<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\StudentPayment;

use App\Models\Course;

use App\Models\User;

use DataTables;

use App\Models\Role;

use App\Jobs\StudentPaymentAddJob;

class StudenPaymentController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if ($request->ajax()) {
            $data = StudentPayment::with('student', 'course')
                ->groupBy('student_id', 'course_id')
                ->selectRaw('student_id, course_id, sum(amount) as total_amount')
                ->orderBy('id', 'desc')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('created', function ($row) {
                    if ($row->created_at == date('Y-m-d h:i:s')) {
                        return "<span style='color:red'>werewr</span>";
                    } else {
                        return $row->created_at;
                    }
                })
                ->addColumn('user_id', function ($row) {

                    return '#00' . optional($row->student)->id;;

                })
                ->addColumn('students', function ($row) {
                    return optional($row->student)->name;
                })
                ->addColumn('course_price', function ($row) {
                    return optional($row->course)->currency . " ".  number_format(optional($row->course)->course_price, 2, '.', ',');
                })
                ->addColumn('amount', function ($row) {
                    return optional($row->course)->currency . " ". number_format($row->total_amount, 2, '.', ','); // Use the alias "total_amount" here
                })
                ->addColumn('due', function ($row) {
                    return optional($row->course)->currency . " ". number_format(optional($row->course)->course_price - $row->total_amount, 2, '.', ','); // Wrap subtraction in parentheses
                })
                ->addColumn('courses', function ($row) {
                    return optional($row->course)->course_title;
                })
                ->addColumn('action', function ($row) {
                    $routePayDetails = route('pay_details', ['student_id' => $row->student_id, 'course_id' => $row->course_id]);

                    return '<a href="' . $routePayDetails . '" class="pay-details btn btn-success btn-sm">View Details</a>';
                })


                ->rawColumns(['created','user_id', 'students', 'courses', 'course_price', 'amount', 'due','action'])
                ->make(true);
        }

        return view('dashboard.payment.index');
    }

    public function getStudentsWithDuePayments(){
        $data = StudentPayment::with('student', 'course')
        ->groupBy('student_id', 'course_id')
        ->selectRaw('student_id, course_id, sum(amount) as total_amount')
        ->orderBy('id', 'desc')
        ->get();
        dd($data);
    }

    public function details($student_id,$course_id){
        $data = StudentPayment::with('student','course','paymentBy')
        ->where(['student_id'=>$student_id,'course_id'=>$course_id])
        ->get();
        $total_amount = StudentPayment::
        where(['student_id'=>$student_id,'course_id'=>$course_id])
        ->sum('amount');
        return view('dashboard.payment.edit',compact('data','total_amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student = Role::where('slug','student-role')
        ->join('users','users.role_id','=','roles.id')
        ->select('users.id','users.name')
        ->get();
        $course = Course::where('status',1)->get();
        return view('dashboard.payment.create',compact('student','course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $payment_amount = StudentPayment::where([
            'student_id'=>$request->student_id,
            'course_id'=>$request->course_id,
        ])->sum('amount');

        $count_price = Course::where('id',$request->course_id)
        ->select('course_title','course_price','currency')->first();

        $due = $count_price->course_price - $payment_amount;

        if($due==0){
            return redirect()->back()->with('error','No course due found');
        }

        $payment = new StudentPayment;
        $payment->student_id = $request->student_id;
        $payment->course_id = $request->course_id;
        $payment->payment_by = auth()->user()->id;
        $payment->amount = $request->amount;
        $payment->save();

        StudentPayment::where('id',$payment->id)->update([
            'transaction_id' => rand().$payment->id,
        ]);

        $payment_amount_new = StudentPayment::where([
            'student_id'=>$request->student_id,
            'course_id'=>$request->course_id,
        ])->sum('amount');

        $due_amount = $count_price->course_price - $payment_amount_new;

        //student find
        $user = User::find($request->student_id);

        StudentPaymentAddJob::dispatch($user->name,$user->email,$request->amount,$count_price->course_title,$due_amount,$request->currency);


        return redirect()->route('student-payment.index')->with('success','Payment has been saved successfully.');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        $data = StudentPayment::find($id);

        StudentPayment::where('id',$id)->update([
            'student_id'     => $data->student_id,
            'course_id'      => $data->course_id,
            'transaction_id' => $data->transaction_id,
            'amount'         => $request->amount,
        ]);

        return redirect()->route('student-payment.index')->with('success','Amount has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
