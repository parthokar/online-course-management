<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Jobs\SendEmail;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendUserEmail;

use App\Models\Role;
use App\Models\Course;
use DB;

class EmailController extends Controller
{
    public function index()
    {


        $role = Role::orderBy('id', 'DESC')->get();
        $course = Course::orderBy('id', 'DESC')->get();
        return view('dashboard.email.create', compact('role', 'course'));
    }

    public function store(Request $request)
    {

        $subject = $request->subject;
        $body = $request->body;

        if ($request->role_id==0) {
            $data = DB::table('roles')
            ->join('users','users.role_id','=','roles.id')
            ->select('users.name', 'email')->get();
            SendEmail::dispatch($data,$subject,$body);
        }

        if (isset($request->role_id)) {
            $data = DB::table('users')->where('role_id', $request->role_id)->select('name', 'email')->get();
            SendEmail::dispatch($data,$subject,$body);
        }

        if ($request->course_id==0) {
            $data = DB::table('course_enrolls')
                ->join('users', 'users.id', '=', 'course_enrolls.student_id')
                ->select('name', 'email')
                ->get();
                SendEmail::dispatch($data,$subject,$body);
        }

        if (isset($request->course_id)) {
            $data = DB::table('course_enrolls')->where('course_id', $request->course_id)
                ->join('users', 'users.id', '=', 'course_enrolls.student_id')
                ->select('name', 'email')
                ->get();
                SendEmail::dispatch($data,$subject,$body);
        }

        if (isset($request->email)) {
            $item = DB::table('users')->where('email',$request->email)->select('name', 'email')->get();
            SendEmail::dispatch($item,$subject,$body);
        }

        return redirect()->back()->with('success','Email has been sent successfully');

    }
}
