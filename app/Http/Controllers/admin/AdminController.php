<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LogActivity;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\User;
use App\Models\Role;


class AdminController extends Controller
{

    //admin dashboard
    public function index(){

        $role = User::where('role_id',auth()->user()->role_id)
        ->select('slug')
        ->join('roles','roles.id','=','users.role_id')
        ->first();

        $instructor_role = Role::where('slug','instructor-role')->first();
        $student_role = Role::where('slug','student-role')->first();

        if($role->slug=='student-role'){
            return redirect()->route('p_s_c_route');
        }

        if($role->slug=='instructor-role'){
            $data['total_course']    = Course::where('teacher_id',auth()->user()->id)->count();
        }else{
            $data['total_course']    = Course::count();
        }

        $data['total_student']   = User::
        join('roles','roles.id','users.role_id')
        ->where('slug','student-role')
        ->count();
        $data['total_teacher']   =
        User::
        join('roles','roles.id','users.role_id')
        ->where('slug','instructor-role')
        ->count();
        $data['total_category']  = CourseCategory::count();
        return view('dashboard.admin.admin-dashboard',compact('data','instructor_role','student_role'));
    }

    //teacher dashboard
    public function teacherDashboard(){
        return view('dashboard.teacher.teacher-dashboard');
    }
}
