<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionQuiz;
use App\Models\Quiz;
use App\Models\Course;
use App\Models\StudentQuiz;
use DB;
use Carbon\Carbon;


class StudentQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $exam_details = Quiz::find($request->quiz_id);
        $dateTime = $exam_details->quiz_date . ' ' . $exam_details->quiz_time;


        //check quiz date is current date to perform quiz
        if (date('Y-m-d') != $exam_details->quiz_date) {
            return redirect()->route('c_q', [
                'course_id' => $request->course_id,
                'quiz_id' => $request->quiz_id,
            ])->with('error', 'You can not attend quiz today because the quiz date is ' . $exam_details->quiz_date);
        }

        $count = StudentQuiz::where('user_id',auth()->user()->id)
        ->where('course_id',$request->course_id)
        ->where('quiz_id',$request->quiz_id)
        ->whereDate('quiz_time', date('Y-m-d'))
        ->count();

        if($count>0){
            return redirect()->route('c_q', [
                'course_id' => $request->course_id,
                'quiz_id' => $request->quiz_id,
            ])->with('error', 'You already submit answer.');;
        }

        // Assuming $request->question_id is an array of question IDs and $request->answer_6, $request->answer_7, etc. exist
        foreach ($request->question_id as $key => $questionId) {
            // Construct the answer key dynamically based on the question ID
            $answer_key = 'answer_' . $questionId;

            // Check if the answer key exists in the request before accessing it
            if ($request->has($answer_key)) {
                // Get the answer and corresponding question text
                $answer = $request->input($answer_key);
                $question = $request->input('question_' . $questionId); // Assuming your question input keys are like "question_6", "question_7", etc.

                // The rest of your code remains the same
                $correct_answer = DB::table('question_quizzes')
                    ->where('id', $questionId)
                    ->first();

                $answer_data = new StudentQuiz;
                $answer_data->user_id = auth()->user()->id;
                $answer_data->course_id = $request->course_id;
                $answer_data->quiz_id = $request->quiz_id;
                $answer_data->question = $request->question[$key];
                $answer_data->question_id = $questionId;
                $answer_data->answer = $answer;
                $answer_data->quiz_time = $request->quiz_time;
                $answer_data->quiz_name = $request->quiz_name;
                $answer_data->correct_answer = $correct_answer->answer;
                $answer_data->status = 1;
                $answer_data->save();
            } else {
                // Handle the case where the answer key doesn't exist
                // You might log an error or perform some other action
            }
        }
         return redirect()->route('get_quiz_result',$request->course_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $quiz = Quiz::where('id', $id)->first();
        $course = Course::where('id', $quiz->course_id)->first();
        $data = QuestionQuiz::where('quiz_id', $id)->where('course_id', $quiz->course_id)->get();
        $answer = StudentQuiz::where('user_id', auth()->user()->id)
            ->where('quiz_id', $id)
            ->where('quiz_time', $quiz->quiz_date . ' ' . $quiz->quiz_time)
            ->where('status', 1)
            ->count();

        $dateTime = $quiz->quiz_date . ' ' . $quiz->quiz_time;

        //    if (Carbon::now()->toDateTimeString() >= $dateTime) {
        //        return redirect()->route('course-enroll.index')->with('error', 'Quiz Time expire');
        //    }

        if ($answer > 0) {
            return redirect()->route('course-enroll.index')->with('error', 'You already submit answer');
        }

        return view('dashboard.student.quiz.create_quiz', compact('quiz', 'course', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
