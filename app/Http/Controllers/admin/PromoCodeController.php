<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PromoCode;
use DataTables;

class PromocodeController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data = PromoCode::orderBy('id','desc')->get();
        if ($request->ajax()) {
            $data = PromoCode::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })

                      ->addColumn('price', function($row){
                        return $row->currency.' '. number_format($row->amount, 2, '.', ',');
                      })

                    ->addColumn('action', function($row){
                        return '<a href="' . route('promo-code.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['price','action'])
                    ->make(true);
        }
        return view('dashboard.promocode.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.promocode.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|unique:promo_codes,code',
            'amount' => 'required|numeric',
            'date_time' => 'required',
        ]);



        PromoCode::create($request->post());

        return redirect()->route('promo-code.index')->with('success','Code has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $category=PromoCode::find($id);
        return view('dashboard.promocode.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category = PromoCode::find($id);

        $request->validate([
            'code' => 'required|unique:promo_codes,code,'.$category->id,
            'amount' => 'required|numeric',
            'date_time' => 'required',
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('promo-code.index')->with('success','Code Has Been updated successfully');
    }

}
