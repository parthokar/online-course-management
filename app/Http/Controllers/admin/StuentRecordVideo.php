<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RecordVideo;
use App\Models\Course;
use DataTables;

class StuentRecordVideo extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = RecordVideo::orderBy('id', 'desc')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('video', function ($row) {
                    // Generate the video file path
                    $videoPath = public_path('dashboard/record_video/' . $row->video_path);

                    // Check if the video file exists
                    if (file_exists($videoPath)) {
                        // Check if the browser supports the video tag
                        $videoSupport = '<video width="320" height="240" controls>' .
                            '<source src="' . asset('dashboard/record_video/' . $row->video_path) . '" type="video/mp4">' .
                            'Your browser does not support the video tag.' .
                            '</video>';

                        // Generate the fallback iframe tag
                        $iframeFallback = '<iframe width="320" height="240" src="' . asset('dashboard/record_video/' . $row->video_path) . '"></iframe>';

                        // Use a conditional statement to decide which to display
                        return '<!--[if IE]>' . $iframeFallback . '<![endif]-->' . $videoSupport;
                    } else {
                        // Display a message if the video file doesn't exist
                        return 'Video not found';
                    }
                })


                ->addColumn('video_link', function ($row) {

                        // Generate HTML for the link to the video file
                        return '<a target="_blank" href="' . asset('dashboard/record_video/' . $row->video_path) . '">View Video</a>';

                })


                ->addColumn('action', function ($row) {
                    return '<a href="' . route('record-video.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })
                ->rawColumns(['video','video_link','action'])
                ->make(true);
        }
        return view('dashboard.video_record.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = Course::all();
        return view('dashboard.video_record.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Get the file from the request
        $image = $request->file('video');

        // Generate a unique name for the file to avoid conflicts
        $filename = time() . '.' . $image->getClientOriginalExtension();

        // Save the image to the public folder
        $image->move('dashboard/record_video/', $filename);

        $data = new RecordVideo();
        $data->course_id = $request->course_id;
        $data->video_path = $filename;
        // $data->video_url = $request->video_url;
        $data->video_title = $request->video_title;
        $data->publish_date = $request->publish_date;
        $data->save();
        return redirect()->route('record-video.index')->with('success', 'Video has been created successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseCategory  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::all();
        $video = RecordVideo::find($id);
        return view('dashboard.video_record.edit', compact('video','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category=RecordVideo::find($id);

        if ($request->hasFile('video')) {
                  // Get the file from the request
            $image = $request->file('video');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/record_video/', $filename);
        }else{
            $filename = $category->video_path;
        }

        RecordVideo::where('id',$id)->update([
            'course_id'=>$request->course_id,
            'video_path'=>$filename,
            // 'video_url' => $request->video_url,
            'video_title' => $request->video_title,
            'publish_date' => $request->publish_date
        ]);

        return redirect()->route('record-video.index')->with('success', 'Video Has Been updated successfully');
    }

}
