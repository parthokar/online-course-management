<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CourseCategory;
use DataTables;

class CourseCategoryController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = CourseCategory::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('course-category.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.teacher.course.course_category.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.teacher.course.course_category.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'course_category' => 'required|unique:course_categories,course_category',
        ]);

        CourseCategory::create($request->post());

        return redirect()->route('course-category.index')->with('success','Category has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $category=CourseCategory::find($id);
        return view('dashboard.teacher.course.course_category.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=CourseCategory::find($id);
        $request->validate([
            'course_category' => 'required|unique:course_categories,course_category,'.$category->id
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('course-category.index')->with('success','Category Has Been updated successfully');
    }

}
