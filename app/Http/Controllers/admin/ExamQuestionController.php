<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Exam;

class ExamQuestionController extends Controller
{


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (isset($request->questions)) {
            $course = Exam::find($request->exam_id);
            $questions = $request->input('questions');
            $options = $request->input('options');
            $answers = $request->input('answers');
            $answers_new = $request->input('answer_new');
            foreach ($questions as $index => $question) {
                $examQuestion = new Question();
                $examQuestion->course_id = $course->course_id; // Replace with the actual exam ID
                $examQuestion->exam_id = $request->exam_id; // Replace with the actual exam ID
                $examQuestion->question = $question;

                // Check if options for this question are available
                if (isset($options[$index])) {
                    $examQuestion->options = json_encode($options[$index]);
                }

                // Check if answer for this question is available
                if (isset($answers[$index])) {
                    // Assuming there's only one answer for each question
                    $examQuestion->answer = $answers[$index][0]; // Get the first element of the array
                }

                if (isset($answers_new[$index])) {
                    // Assuming there's only one answer for each question
                    $examQuestion->answer = $answers_new[$index][0]; // Get the first element of the array
                }

                if(isset($request->option_type)){
                    $examQuestion->option_type = $request->option_type[$index];
                }

                $examQuestion->save();
            }
        }
        return redirect()->route('exam.edit',$id)->with('success','Exam has been updated successfully.');
    }




}
