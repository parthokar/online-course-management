<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quiz;
use App\Models\Course;
use App\Models\QuestionQuiz;
use DataTables;
use DB;


class QuizController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Quiz::leftjoin('courses','courses.id','=','quizzes.course_id')->select('course_title','quizzes.*')->orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    // ->addColumn('course_name', function($row){
                    //     return optional($row->courseName->course_title);
                    //  })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('quiz.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                     ->rawColumns(['action',])
                    ->make(true);
        }
        return view('dashboard.teacher.quiz.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $course = Course::where('status',1)->orderBy('id','DESC')->get();
        $quiz = Quiz::orderBy('id','desc')->get();
        return view('dashboard.teacher.quiz.create',compact('course','quiz'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'course_id' => 'required',
            'title' => 'required',
            'quiz_date' => 'required',
            'quiz_time' => 'required',
            'details' => 'required',
        ]);

        Quiz::create($request->post());

        return redirect()->route('quiz.create')->with('success','Quiz has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Quiz  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $course = Course::where('status',1)->orderBy('id','DESC')->get();
        $category=Quiz::with('questionData')->find($id);
        $quiz = Quiz::orderBy('id','desc')->get();
        $question = QuestionQuiz::where('quiz_id',$id)->where('course_id',$category->course_id)->get();
        return view('dashboard.teacher.quiz.edit',compact('question','course','category','quiz'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=Quiz::find($id);
        $request->validate([
            'course_id' => 'required',
            'title' => 'required',
            'quiz_date' => 'required',
            'quiz_time' => 'required',
            'details' => 'required',
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('quiz.edit',$id)->with('success','Quiz Has been updated successfully');
    }

    public function quizQDelete($id){
        DB::table('question_quizzes')->where('id',$id)->delete();
        return back();
    }


}
