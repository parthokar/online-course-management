<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LogActivity;
use DataTables;

class StudentLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = LogActivity::with('student')->orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()

                    ->addColumn('created', function($row){
                         return date('Y-m-d h:i:a',strtotime($row->created_at));
                     })

                     ->addColumn('students', function($row){
                          return optional($row->student)->name;
                      })

                    // ->addColumn('action', function($row){
                    //     return '<a href="' . route('announcement.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                    //  })

                    ->rawColumns(['created','students'])
                    ->make(true);
        }
        return view('dashboard.student_log.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
