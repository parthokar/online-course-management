<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\PromoCode;
use App\Models\CourseCategory;
use App\Models\Slack;
use App\Models\Role;
use DataTables;

class CourseController extends Controller
{

   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $role = Role::where('id',auth()->user()->role_id)->first();
            if($role->slug=='instructor-role'){
                $data = Course::with('category','created_user')->where('teacher_id',auth()->user()->id)->orderBy('id','desc')->get();
            }else{
                $data = Course::with('category','created_user')->orderBy('id','desc')->get();
            }

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('category', function($row){
                       return $row->category->course_category;
                    })
                    ->addColumn('image', function($row){
                        return '<img src="'.asset('dashboard/course/'.$row->course_image).'" width="50%" height="50%" class="img-fluid">';
                     })
                    ->addColumn('created', function($row){
                        return optional($row->created_user)->name;
                     })

                     ->addColumn('course_price', function($row){
                        return $row->currency.' '. number_format($row->course_price, 2, '.', ',');
                      })


                    ->addColumn('action', function($row){

                            return '<a href="' . route('course.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';

                     })
                    ->rawColumns(['status','category','image','created','course_price','action'])
                    ->make(true);
        }
        return view('dashboard.teacher.course.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $category = CourseCategory::where('status',1)->orderBY('course_category','ASC')->get();
        $promocode = PromoCode::where('status',1)->where('date_time','>=',date('Y-m-d H:i:s'))->get();
        $teacher = Role::where('slug','instructor-role')
        ->join('users','users.role_id','=','roles.id')
        ->select('users.id','users.name')
        ->get();
        return view('dashboard.teacher.course.create',compact('category','promocode','teacher'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'teacher_id' => 'required',
            'course_title' => 'required',
            'tag_line' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'duration' => 'required',
            'details' => 'required',
            'contents' => 'required',
            'requirements' => 'required',
            'course_price' => 'required|numeric',
            'course_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'status' => 'required',
        ]);

        // Get the file from the request
        $image = $request->file('course_image');

        // Generate a unique name for the file to avoid conflicts
        $filename = time().'.'.$image->getClientOriginalExtension();

        // Save the image to the public folder
        $image->move('dashboard/course/', $filename);

       $course = Course::create([
            'category_id'=>$request->category_id,
            'teacher_id'=>$request->teacher_id,
            'promo_id'=>$request->promo_id,
            'course_title'=>$request->course_title,
            'tag_line'=>$request->tag_line,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'duration'=>$request->duration,
            'details'=>$request->details,
            'contents'=>$request->contents,
            'requirements'=>$request->requirements,
            'course_image'=>$filename,
            'course_price'=>$request->course_price,
            'currency'=>$request->currency,
            'free'=>$request->free,
            'status'=>$request->status,
        ]);

        $slack = new Slack;
        $slack->course_id = $course->id;
        $slack->channel_name = $request->course_title.'-'.'Channel'.'-'.$course->id;
        $slack->save();

        return redirect()->route('course.index')->with('success','Course has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $course   = Course::find($id);
        $categorys = CourseCategory::where('status',1)->get();
        $promocode = PromoCode::where('status',1)->where('date_time','>=',date('Y-m-d H:i:s'))->get();
        $teacher = Role::where('slug','instructor-role')
        ->join('users','users.role_id','=','roles.id')
        ->select('users.id','users.name')
        ->get();
        return view('dashboard.teacher.course.edit',compact('course','categorys','promocode','teacher'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $course=Course::find($id);

        $request->validate([
            'category_id' => 'required',
            'teacher_id' => 'required',
            'course_title' => 'required',
            'tag_line' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'duration' => 'required',
            'details' => 'required',
            'contents' => 'required',
            'requirements' => 'required',
            'course_price' => 'required|numeric',
            'course_image' => 'image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'status' => 'required',
        ]);

        if ($request->hasFile('course_image')) {
                  // Get the file from the request
            $image = $request->file('course_image');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/course/', $filename);
        }else{
            $filename = $course->course_image;
        }

        Course::where('id',$id)->update([
            'category_id'=>$request->category_id,
            'teacher_id'=>$request->teacher_id,
            'promo_id'=>$request->promo_id,
            'live_class_url'=>$request->live_class_url,
            'course_title'=>$request->course_title,
            'tag_line'=>$request->tag_line,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'duration'=>$request->duration,
            'details'=>$request->details,
            'contents'=>$request->contents,
            'requirements'=>$request->requirements,
            'course_image'=>$filename,
            'course_price'=>$request->course_price,
            'currency'=>$request->currency,
            'free'=>$request->free,
            'status'=>$request->status,
        ]);
        return redirect()->route('course.index')->with('success','Course has been updated successfully.');
    }





  //promocode.......................
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function pindex(Request $request)
    {
        if ($request->ajax()) {
            $data = PromoCode::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })

                    ->addColumn('amount', function($row){
                        return $row->currency.' '. number_format($row->amount, 2, '.', ',');
                      })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('promo_edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['amount','action'])
                    ->make(true);
        }
        return view('dashboard.promocode.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function pcreate()
    {
        return view('dashboard.promocode.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function pstore(Request $request)
    {
        $request->validate([
            'code' => 'required|unique:promo_codes,code',
            'amount' => 'required|numeric',
            'date_time' => 'required',
        ]);

        PromoCode::create($request->post());

        return redirect()->route('promo_index')->with('success','Code has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function pedit($id)
    {

        $category=PromoCode::find($id);
        return view('dashboard.promocode.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function pupdate(Request $request,$id)
    {
        $category = PromoCode::find($id);

        $request->validate([
            'code' => 'required|unique:promo_codes,code,'.$category->id,
            'amount' => 'required|numeric',
            'date_time' => 'required',
        ]);

        $category->fill($request->post())->save();

        return redirect()->route('promo_index')->with('success','Code Has Been updated successfully');
    }

}
