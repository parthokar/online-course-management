<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Announcement;
use DataTables;

class AnnouncementController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Announcement::orderBy('id', 'desc')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                    return '<img src="'.asset('dashboard/announce/'.$row->image).'" width="80%" height="80%" class="img-fluid">';
                })
                ->addColumn('created', function ($row) {
                    return $row->created_user->name;
                })
                ->addColumn('details', function ($row) {
                    return strip_tags($row->details);
                })
                ->addColumn('action', function ($row) {
                    $editUrl = route('announcement.edit', $row->id);
                    $deleteUrl = route('announcement.destroy', $row->id);

                    // Add the delete button
                    return '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm">Edit</a>
                            <form action="' . $deleteUrl . '" method="POST" class="d-inline">
                                ' . csrf_field() . '
                                ' . method_field('DELETE') . '
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>';
                })
                ->rawColumns(['image', 'created', 'action'])
                ->make(true);
        }

        return view('dashboard.teacher.announcement.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.teacher.announcement.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

        if($request->image!=null){
            $request->validate([
                'topics' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'details' => 'required',
            ]);

            // Get the file from the request
            $image = $request->file('image');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/announce/', $filename);
        }else{
            $request->validate([
                'topics' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'details' => 'required',
            ]);

            $filename = null;

        }


        Announcement::create([
            'topics'=>$request->topics,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'course_price'=>$request->course_price,
            'image'=>$filename,
            'url'=>$request->url,
            'details'=>$request->details,
        ]);

        return redirect()->route('announcement.index')->with('success','Announcement has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Announcement  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $category=Announcement::find($id);
        return view('dashboard.teacher.announcement.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=Announcement::find($id);
        $request->validate([
            'topics' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'details' => 'required',
        ]);

        if ($request->hasFile('image')) {
                  // Get the file from the request
            $image = $request->file('image');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/announce/', $filename);
        }else{
            $filename = $category->image;
        }

        Announcement::where('id',$id)->update([
            'topics'=>$request->topics,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'image'=>$filename,
            'url'=>$request->url,
            'details'=>$request->details,
        ]);

        return redirect()->route('announcement.index')->with('success','Announcement Has Been updated successfully');
    }

    public function destroy($id)
    {
        // Find the announcement by ID
        $announcement = Announcement::findOrFail($id);

        // If the announcement is not found, return a response with a 404 status code


        // Delete the announcement
        $announcement->delete();

        // Return a response indicating successful deletion

        return redirect()->route('announcement.index')->with('success','Announcement deleted successfully');
    }
}
