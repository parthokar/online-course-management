<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;
use DataTables;

class VideoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Video::orderBy('id', 'desc')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('video_type', function ($row) {
                    if ($row->video_type == 1) {
                        return 'Free Video';
                    } else {
                        return 'Recorder Video';
                    }
                })
                ->addColumn('publish_date', function ($row) {
                    return date('d-m-Y H:i:s a', strtotime($row->publish_date));
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return 'Active';
                    } else {
                        return 'InActive';
                    }
                })
                ->addColumn('action', function ($row) {
                    return '<a href="' . route('video.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })
                ->rawColumns(['video_type','publish_date','status','action'])
                ->make(true);
        }
        return view('dashboard.video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            'video_type' => 'required',
            'publish_date' => 'required',
            'status' => 'required',
        ]);

        Video::create($request->post());

        return redirect()->route('video.index')->with('success', 'Video has been created successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseCategory  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $video = Video::find($id);
        return view('dashboard.video.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            'video_type' => 'required',
            'publish_date' => 'required',
            'status' => 'required',
        ]);

        $video->fill($request->post())->save();

        return redirect()->route('video.index')->with('success', 'Video Has Been updated successfully');
    }

}
