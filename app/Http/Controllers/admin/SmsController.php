<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Course;

class SmsController extends Controller
{
    public function index(){
        $role = Role::orderBy('id', 'DESC')->get();
        $course = Course::orderBy('id', 'DESC')->get();
        return view('dashboard.sms.create',compact('role','course'));
    }
}
