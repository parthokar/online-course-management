<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RolePermission;
use App\Models\Role;
use App\Models\Permission;

class RolePermissionController extends Controller
{
    public function index()
    {
        $rolePermissions = RolePermission::all();
        return view('role_permissions.index', compact('rolePermissions'));
    }

    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('role_permissions.create', compact('roles', 'permissions'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'role_id' => 'required',
            'permission_id' => 'required',
        ]);

        RolePermission::create($request->all());

        return redirect()->route('role_permissions.index')->with('success', 'Role permission assigned successfully.');
    }

    public function destroy(RolePermission $rolePermission)
    {
        $rolePermission->delete();

        return redirect()->route('role_permissions.index')->with('success', 'Role permission revoked successfully.');
    }
}
