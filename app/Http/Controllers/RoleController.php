<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Role;

use App\Models\Permission;

use DataTables;

use DB;

use Illuminate\Support\Str;

class RoleController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Role::orderBy('id', 'desc')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return '<a href="' . route('roles.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('dashboard.admin.roles.index');
    }

    public function create()
    {
        $permission = Permission::all();
        return view('dashboard.admin.roles.create', compact('permission'));
    }



    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles|max:255',
            'permissions' => 'required|array', // Make sure the permissions are submitted as an array
        ]);

        // Create the role
        $data = [
            'name' =>$request->name,
            'slug' =>Str::slug($request->name),
        ];
        $role = Role::create($data);

        // Attach the selected permissions to the role
        foreach($request->permissions as $key=> $permission){
            DB::table('role_permissions')->insert([
               'role_id'=>$role->id,
               'permission_url'=>$permission,
            ]);
          }

        return redirect()->route('roles.index')->with('success', 'Role created successfully.');
    }

    public function edit(Role $role)
    {
        $permission = Permission::all();

        $rolePermissions = DB::table('role_permissions')
        ->where('role_id', $role->id)
        ->pluck('permission_url')
        ->toArray();

        return view('dashboard.admin.roles.edit', compact('role', 'permission', 'rolePermissions'));
    }

    public function update(Request $request, Role $role)
    {

        $request->validate([
            'name' => 'required|unique:roles,name,' . $role->id . '|max:255',
            'permissions' => 'required|array',
        ]);

     // Update the role name
     $role->update($request->only('name'));

      DB::table('role_permissions')->where('role_id',$role->id)->delete();

      foreach($request->permissions as $key=> $permission){
        DB::table('role_permissions')->insert([
           'role_id'=>$role->id,
           'permission_url'=>$permission,
        ]);
      }

        return redirect()->route('roles.index')->with('success', 'Role updated successfully.');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('roles.index')->with('success', 'Role deleted successfully.');
    }
}
