<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CourseEnroll;
use App\Models\Course;
use App\Models\Exam;
use App\Models\Quiz;
use DataTables;
use Auth;


class StudentCourseEnroll extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = CourseEnroll::with('course', 'promo')
                ->where('student_id', auth()->user()->id)
                ->orderBy('id', 'desc')->get();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('created', function ($row) {
                    if ($row->created_at == date('Y-m-d h:i:a')) {
                        return "span style='color:red'>werewr</span>";
                    } else {
                        return $row->created_at;
                    }
                })

                ->addColumn('courses', function ($row) {
                    return optional($row->course)->course_title;
                })

                ->addColumn('discount_code', function ($row) {
                    return optional($row->promo)->code;
                })

                ->addColumn('discount_amount', function ($row) {
                    return optional($row->promo)->amount;
                })

                ->addColumn('action', function ($row) {
                    return '<a href="' . route('student-course-enroll.show', $row->id) . '" class="edit btn btn-primary btn-sm">Show</a>';
                })

                ->addColumn('action_exam', function ($row) {
                    $exams = Exam::where('course_id', $row->course_id)->select('id', 'exam_date', 'exam_time')->first();

                    if (isset($exams)) {
                        $dateTime = $exams->exam_date;

                        $currentTime = date('Y-m-d');

                        if ($currentTime == $dateTime) {
                            return '<a href="' . route('student-exam.show', $exams->id) . '" class="edit btn btn-warning btn-sm">Take Exam</a>';
                        }
                    }
                })

                ->addColumn('action_quiz', function ($row) {

                    $quizs = Quiz::where('course_id', $row->course_id)->select('id', 'quiz_date', 'quiz_time')->first();

                    if (isset($quizs)) {
                        $dateTime = $quizs->quiz_date;

                        $currentTime = date('Y-m-d');

                        if ($currentTime == $dateTime) {
                            return '<a href="' . route('student-quiz.show', $quizs->id) . '" class="edit btn btn-success btn-sm">Take Quiz</a>';
                        }
                    }
                })

                ->rawColumns(['created', 'courses', 'discount_code', 'discount_amount', 'action', 'action_exam', 'action_quiz'])
                ->make(true);
        }
        return view('frontend.course.course-enroll');
    }

    //enroll store
    public function store(Request $request)
    {

        if (Auth::check()) {
            $course = Course::find($request->course_id);

            $check = CourseEnroll::where('student_id', auth()->user()->id)
                ->where('course_id', $request->course_id)
                ->count();

            if ($check > 0) {
                return redirect()->route('course_details', $request->course_id)->with('error','Course already enroll');
            }

            $courseEnroll = new CourseEnroll;

            $courseEnroll->student_id = auth()->user()->id;

            $courseEnroll->course_id = $request->course_id;

            $courseEnroll->save();
            return redirect()->route('course_details', $request->course_id)->with('success','Course enroll successfull');
        } else {
            return redirect()->route('course_details', $request->course_id)->with('error','Please sign in first to enroll this course');;
        }

    }

    public function show($id)
    {
        $data = CourseEnroll::with('course', 'promo')
            ->where('id', $id)
            ->orderBy('id', 'desc')->first();
        return view('frontend.course.course-enroll-show', compact('data'));
    }

    //enroll page view
    public function enrollNow()
    {
        return view('frontend.course.course-enroll');
    }


}
