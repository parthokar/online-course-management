<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Announcement;
use App\Models\Video;
use App\Models\Company;
use App\Models\RecordVideo;
use App\Models\CourseEnroll;
use App\Models\StudentPayment;
use App\Models\Quiz;
use App\Models\QuestionQuiz;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Slack;
use App\Models\Notice;
use App\Models\User;
use App\Models\StudentExam;
use App\Models\StudentQuiz;
use App\Jobs\ResetPassword;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DB;
use Auth;


class FrontendController extends Controller
{
    //home page
    public function index()
    {
        $data = Course::orderBy('id', 'desc')->limit(3)->get();
        $free_video = Video::where('video_type', 1)->where('status', 1)->orderBy('id', 'desc')->limit(3)->get();
        $recorded_video = Video::where('video_type', 0)->where('status', 1)->orderBy('id', 'desc')->limit(3)->get();
        $company_logo = Company::where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.index', compact('data', 'free_video', 'recorded_video', 'company_logo'));
    }

    public function allVideo()
    {
        $free_video = Video::where('video_type', 1)->where('status', 1)->orderBy('id', 'desc')->get();
        $recorded_video = Video::where('video_type', 0)->where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.all-video', compact('free_video', 'recorded_video'));
    }

    //announcement
    public function announcement()
    {
        $data = Announcement::orderBy('id', 'desc')->get();
        return view('frontend.announcement', compact('data'));
    }

    //all course
    public function allCourse()
    {
        $data = Course::orderBy('id', 'desc')->get();
        return view('frontend.course.all-course', compact('data'));
    }

    public function recordVideo()
    {
        $data = RecordVideo::
            join('course_enrolls', 'course_enrolls.course_id', '=', 'record_videos.course_id')
            ->join('courses', 'courses.id', '=', 'record_videos.course_id')
            ->where('course_enrolls.student_id', auth()->user()->id)
            ->select('record_videos.*', 'courses.course_title')
            ->get();
        return view('frontend.record-video',compact('data'));
    }

    //about page
    public function about()
    {
        return view('frontend.about');
    }

    //contact page
    public function contact()
    {
        return view('frontend.contact');
    }

    //faq page
    public function faq()
    {
        return view('frontend.faq');
    }

    //course details
    public function show($id)
    {
        $data = Course::with('category')->find($id);
        return view('frontend.course.course-details', compact('data'));
    }


    public function studentPanelCourse(){
        $data = CourseEnroll::with('student','course','promo')
        ->where('student_id',auth()->user()->id)->orderBy('id','desc')
        ->get();
        return view('frontend.student_panel.student_courses',compact('data'));
    }

    public function studentPanelCourseDetails($id){
        $payment_amount = StudentPayment::where([
            'student_id'=>auth()->user()->id,
            'course_id'=>$id,
        ])->sum('amount');

        if($payment_amount==0){
            return redirect()->back()->with('error','You have Payment due. Clear dues to access course materials.');
        }
        $course = Course::find($id);
        return view('frontend.student_panel.student_selected_course',compact('course'));
    }

    public function courseSylabus($id){
        $course = Course::find($id);
        return view('frontend.student_panel.course-sylabus',compact('course'));
    }

    public function classNews($id){
        $course = Course::find($id);
        $notice = Notice::where('course_id',$id)->where('status',1)->orderBy('id','DESC')->get();
        return view('frontend.student_panel.course-class-news',compact('course','notice'));
    }

    public function courseRecording($id){

        $course = Course::find($id);

        $data = RecordVideo::
             where('course_id',$id)
             ->join('courses', 'courses.id', '=', 'record_videos.course_id')
            ->select('record_videos.*', 'courses.course_title')
            ->get();
        return view('frontend.student_panel.course-recorded-video',compact('course','data'));
    }

    public function courseTestAndQuiz($id){
        $course = Course::find($id);
        $exam = Exam::where('course_id',$id)->get();
        $quiz = Quiz::where('course_id',$id)->get();
        return view('frontend.student_panel.test-quiz',compact('course','exam','quiz'));
    }

    public function courseMeetTeacher($id){
        $course = Course::find($id);
        $instructor = Course::where('teacher_id',$course->teacher_id)
        ->join('users','users.id','=','courses.teacher_id')
        ->select('users.id','users.name','users.email')
        ->first();
        return view('frontend.student_panel.meet-teacher',compact('course','instructor'));
    }

    public function coursePaymentInvoice($id){

        $course = Course::find($id);

        $data = StudentPayment::with('student','course','paymentBy')
        ->where(['student_id'=>auth()->user()->id,'course_id'=>$id])
        ->get();

        $total_amount = StudentPayment::
        where(['student_id'=>auth()->user()->id,'course_id'=>$id])
        ->sum('amount');
        return view('frontend.student_panel.payment-invoice',compact('course','data','total_amount'));
    }

    public function courseJoinLive($id){
        $course = Course::find($id);
        return view('frontend.student_panel.student_selected_course',compact('course'));
    }



    public function courseQuiz($course_id,$quiz_id){

        $quiz = Quiz::where('id',$quiz_id)->first();
        $course = Course::where('id',$course_id)->first();
        $data = QuestionQuiz::where('course_id',$course_id)
        ->where('quiz_id',$quiz_id)
        ->get();

        return view('frontend.student_panel.quiz-questions',compact('quiz','course','data'));
    }


    public function courseTest($course_id,$test_id){

        $exam = Exam::where('id',$test_id)->first();
        $course = Course::where('id',$course_id)->first();
        $data = Question::where('course_id',$course_id)
        ->where('exam_id',$test_id)
        ->get();
        return view('frontend.student_panel.exam-questions',compact('exam','course','data'));
    }


    public function chatStudent($id){
        $course = Course::where('id',$id)->first();
        $data = Slack::where('course_id',$id)
        ->get();
        return view('frontend.student_panel.chat-student',compact('course','data'));
    }

    public function chatStudentMessage($id,$course_id){
        $course = Course::where('id',$course_id)->first();
        $channel_info = Slack::where('id',$id)->first();

        if($channel_info->channel_type==2){
            return view('frontend.student_panel.chat-message',compact('course','id','channel_info'));
        }
        if($channel_info->channel_type==1){
           $course_enroll = CourseEnroll::
           where('course_id',$channel_info->course_id)
           ->where('student_id',auth()->user()->id)
           ->first();
           if(!isset($course_enroll)){
             return redirect()->back()->with('error','Channel is restricted');
           }else{
            return view('frontend.student_panel.chat-message',compact('course','id','channel_info'));
           }
        }
        return view('frontend.student_panel.chat-message',compact('course','id','channel_info'));
    }

    public function userRegistration(){
        return view('frontend.registration');
    }

    public function userResetPassword(){
        return view('frontend.reset-password');
    }

    public function userResetPasswordPost(Request $request){
       $check_email = User::where('email',$request->email)->count();
       if($check_email==0){
         return redirect()->back()->with('error','If your email exists then we will send you reset password link');
       }else{
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now(),
        ]);

        ResetPassword::dispatch($request->email,$token);
        return redirect()->back()->with('success','Please check your email for reset password link');

       }
    }


    public function getExamResult($course_id){
        $student = User::find(auth()->user()->id);
        $course = Course::where('id',$course_id)->first();
        $exam   = StudentExam::where('student_exams.course_id',$course_id)
        ->select('exam_name as title','exam_time')
                 ->first();
        $data = StudentExam::
        whereYear('exam_time',date('Y'))
        ->whereMonth('exam_time',date('m'))
        ->where('course_id',$course_id)
        ->where('user_id',auth()->user()->id)
        ->select('student_exams.question','correct_answer as question_answer','student_exams.answer as student_answer')
        ->groupBy('student_exams.id')
        ->selectRaw('SUM(CASE WHEN student_exams.answer = student_exams.correct_answer THEN 1 ELSE 0 END) as total_correct_answers')
        ->selectRaw('SUM(CASE WHEN student_exams.answer != student_exams.correct_answer THEN 1 END) as total_wrong_answers')
        ->get();

        $score = $data->sum('total_correct_answers');
        $wrong = $data->sum('total_wrong_answers');


        if($data->count()==0){
           return redirect()->back()->with('error','No Exam Record Found');
        }

        return view('dashboard.certificate.certificate-download',compact('course','exam','data','student','score','wrong'));
    }

    public function getQuizResult($course_id){
        $student = User::find(auth()->user()->id);
        $course = Course::where('id',$course_id)->first();
        $exam   = StudentQuiz::where('student_quizzes.course_id',$course_id)
                  ->select('quiz_name as title','quiz_time')
                 ->first();
                 $data = StudentQuiz::
                 whereYear('quiz_time',date('Y'))
                 ->whereMonth('quiz_time',date('m'))
                 ->where('course_id',$course_id)
                 ->where('user_id',auth()->user()->id)
                 ->select('student_quizzes.question','student_quizzes.answer as student_answer','correct_answer as question_answer')
                 ->groupBy('student_quizzes.id')
                 ->selectRaw('SUM(CASE WHEN correct_answer = student_quizzes.answer THEN 1 ELSE 0 END) as total_correct_answers')
                 ->selectRaw('SUM(CASE WHEN correct_answer != student_quizzes.answer THEN 1 END) as total_wrong_answers')
                ->get();


        $score = $data->sum('total_correct_answers');
        $wrong = $data->sum('total_wrong_answers');


        if($data->count()==0){
            return redirect()->back()->with('error','No Quiz Record Found');
        }

        return view('dashboard.certificate.certificate-download-quiz',compact('course','exam','data','student','score','wrong'));
    }



}
