<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MeetTeacherStudentRequest;
use App\Models\MeetTeacher;
use App\Models\User;
use App\Models\Course;
use App\Models\CourseEnroll;
use DataTables;

class StudentMeetRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = MeetTeacherStudentRequest::
            join('users','users.id','=','meet_teacher_student_requests.student_id')
            ->where('teacher_id',auth()->user()->id)
            ->orderBy('meet_teacher_student_requests.id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()

                    ->addColumn('student', function($row){
                        return $row->name;
                     })


                    ->addColumn('request_time', function($row){
                        return $row->request_time;
                     })


                    // ->addColumn('action', function($row){
                    //     return '<a href="' . route('meet-instructor.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                    //  })

                    ->rawColumns(['student','request_time'])
                    ->make(true);
        }
        return view('dashboard.student.teacher_meet.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $time    = MeetTeacher::all();
        $teacher = User::where('role_id',3)->get();
        return view('dashboard.student.teacher_meet.create',compact('time','teacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $check = MeetTeacherStudentRequest::where('student_id',auth()->user()->id)
        ->where('teacher_id',$request->teacher_id)
        ->whereDate('created_at',date('Y-m-d'))
        ->count();

        if($check>0){
           return redirect()->route('m_t',$request->course_id)
           ->with('error','already request submit');
        }



        $meet = new MeetTeacherStudentRequest;

        $meet->student_id = auth()->user()->id;

        $meet->teacher_id  =  $request->teacher_id;

        $meet->request_time  =  $request->request_time;

        $meet->save();

        return redirect()->route('m_t',$request->course_id)
        ->with('success','Request submit successfull.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data =  MeetTeacherStudentRequest::find($id);
       $time    = MeetTeacher::all();
       $teacher = User::where('role_id',3)->get();
       return view('dashboard.student.teacher_meet.edit',compact('data','time','teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meet = MeetTeacherStudentRequest::find($id);

        $meet->student_id = auth()->user()->id;

        $meet->teacher_id  =  $request->teacher_id;

        $meet->time_id  =  $request->time_id;

        $meet->save();

        return redirect()->route('meet-instructor.index')
        ->with('success','Request submit successfull.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function certificate(){
        $course = Course::latest()->get();
        return view('dashboard.certificate_data.index',compact('course'));

    }

    public function certificateData(Request $request){
         $course_data = CourseEnroll::
         where('course_id',$request->course_id)
         ->where('student_id',$request->student_id)
         ->where('is_download_certificate',1)
         ->join('courses','courses.id','=','course_enrolls.course_id')
         ->first();

         if(isset($course_data)){
            $course_data = CourseEnroll::
            where('course_id',$request->course_id)
            ->where('student_id',$request->student_id)
            ->where('is_download_certificate',1)
            ->join('courses','courses.id','=','course_enrolls.course_id')
            ->first();
            $users = User::where('id',$request->student_id)->select('name')->first();
            return view('dashboard.certificate_data.download',compact('course_data','users'));
         }

         else{
            return redirect()->back()->with('error','No certificate Found');
         }


    }
}
