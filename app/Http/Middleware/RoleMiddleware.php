<?php

// app/Http/Middleware/RoleMiddleware.php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use App\Models\Role;
use DB;

class RoleMiddleware
{
    public function handle($request, Closure $next, ...$requiredRoles)
    {
        // Check if the user is authenticated
        if (!auth()->check()) {
            return redirect()->route('home_route');
        }

        // Get the user's role_id
        $userRoleId = auth()->user()->role_id;

        $fullUrl = \Route::currentRouteName();

       //dd($fullUrl);

        $checkPermission = DB::table('role_permissions')
        ->where('role_id',$userRoleId)
        ->where('permission_url',$fullUrl)
        ->first();

        if(!isset($checkPermission)){
            abort(403, 'You dont have permission to access this page.');
        }


        return $next($request);
    }
}



