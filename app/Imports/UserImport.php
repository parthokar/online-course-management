<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Role;
use App\Jobs\ResetPassword;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DB;


class UserImport implements ToModel, WithHeadingRow
{
    public $errorMessages = [];

    private $rowNumber = 1;

    public function model(array $row)
    {
        $name = $row['name'];
        $email = $row['email'];

        $existingUser = User::where('email', $email)->first();

        if ($existingUser) {
            $errorMessage = "Email already exists in row {$this->rowNumber}: {$email}";
            $this->errorMessages[] = $errorMessage;
        } else {
            $student = Role::where('slug', 'student-role')
                ->select('roles.id')
                ->first();

            $token = Str::random(64);
            DB::table('password_resets')->insert([
                'email' => $email,
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);

            ResetPassword::dispatch($email,$token);

            return new User([
                'name' => $name,
                'email' => $email,
                'role_id' => $student->id,
                'status' => 1,
            ]);
        }


    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }
}
