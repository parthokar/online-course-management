<?php

namespace App\Console\Commands;

use App\Models\StudentPayment;
use App\Mail\PaymentReminderEmail;
use Illuminate\Console\Command;
use App\Jobs\SendPaymentReminderJob;
use Illuminate\Support\Facades\Mail;


class SendPaymentReminders extends Command
{
    protected $signature = 'send:payment-reminders';
    protected $description = 'Send email reminders to students with due payments';

    public function handle()
    {


        $data = StudentPayment::selectRaw('
        currency,
        email,
        name,
        student_payments.student_id,
        student_payments.course_id,
        courses.course_price,
        SUM(student_payments.amount) as total_amount,
        (courses.course_price - SUM(student_payments.amount)) as due_amount
    ')
            ->leftJoin('users', 'users.id', '=', 'student_payments.student_id')
            ->leftJoin('courses', 'courses.id', '=', 'student_payments.course_id')
            ->groupBy('student_payments.student_id', 'student_payments.course_id', 'courses.course_price')
            ->havingRaw('(courses.course_price - SUM(student_payments.amount)) > 0')
            ->get();


        foreach ($data as $student) {
            Mail::to($student->email)->send(new PaymentReminderEmail($student));
        }


        $this->info('Payment reminders sent successfully!');
    }
}
