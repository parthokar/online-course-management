<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\StudentAddPaymentMail;
use Illuminate\Support\Facades\Mail;

class StudentPaymentAddJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $name;
    public $email;
    public $amount;
    public $course;
    public $due_amount;
    public $currency;


    public function __construct($name,$email,$amount,$course,$due_amount,$currency)
    {
        $this->name = $name;
        $this->email = $email;
        $this->amount = $amount;
        $this->course = $course;
        $this->due_amount = $due_amount;
        $this->currency = $currency;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::to($this->email)->send(new StudentAddPaymentMail($this->name,$this->email,$this->amount,$this->course,$this->due_amount,$this->currency));
    }
}
