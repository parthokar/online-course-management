<?php

namespace App\Jobs;

use App\Mail\PaymentReminderEmail;
use App\Models\StudentPayment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPaymentReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $student;

    public function __construct($student)
    {
        $this->student = $student;
    }

    public function handle()
    {
        Mail::to($this->student->email)->send(new PaymentReminderEmail($this->student));
    }
}
